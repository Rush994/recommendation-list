package com.smedialink.recommendation_list.presentation.feature.recommendationsingle

import android.graphics.drawable.Animatable
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.smedialink.recommendation_list.R
import com.smedialink.recommendation_list.databinding.ActivityRecommendationSingleBinding
import com.smedialink.recommendation_list.presentation.base.BaseActivity
import com.smedialink.recommendation_list.presentation.base.Constants
import com.smedialink.recommendation_list.presentation.base.hideEditText
import com.smedialink.recommendation_list.presentation.base.interfaces.OnTipItemClickListener
import com.smedialink.recommendation_list.presentation.base.revealEditText
import com.smedialink.recommendation_list.presentation.feature.recommendationsingle.adapter.RecommendationSingleTagsRecyclerAdapter
import com.smedialink.recommendation_list.presentation.feature.recommendationsingle.adapter.RecommendationSingleTipsRecyclerAdapter
import com.smedialink.recommendation_list.presentation.feature.recommendationsingle.viewmodel.RecommendationSingleViewModel
import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationTipViewModel
import javax.inject.Inject

class RecommendationSingleActivity : BaseActivity(R.layout.activity_recommendation_single),
    OnTipItemClickListener {

    private val binding by viewBinding<ActivityRecommendationSingleBinding>()

    @Inject
    lateinit var viewModel: RecommendationSingleViewModel

    private val layoutManagerHorizontal = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
    private val layoutManagerVertical = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
    private lateinit var tagsAdapter: RecommendationSingleTagsRecyclerAdapter
    private lateinit var tipsAdapter: RecommendationSingleTipsRecyclerAdapter
    private var isEditTextTipVisible: Boolean = false
    private var isEditTextTagVisible: Boolean = false

    override fun initListeners() {
        viewModel.recTipLiveDate.observe(
            this
        ) { item ->
            with(binding) {
                tvSingleIdValue.text = item.single.id.toString()
                tvSingleHeaderValue.text = item.single.header
                tvSingleCategoryValue.text = item.single.category
            }
            tipsAdapter.setTipList(item.tips)
        }

        viewModel.tagLiveData.observe(
            this
        ) { list ->
            tagsAdapter.setTipList(list)
        }

        viewModel.isDeleteLiveData.observe(
            this
        ) { item ->
            if (item) {
                Toast.makeText(this, "Recommendation deleted", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    override fun initData() {
        viewModel.selectedId = intent.getLongExtra(Constants.RECOMMENDATION_ID, 0)
        tipsAdapter = RecommendationSingleTipsRecyclerAdapter()
        val dividerItemDecoration = DividerItemDecoration(this, layoutManagerVertical.orientation)

        with(binding) {
            rvSingleTips.addItemDecoration(dividerItemDecoration)
            rvSingleTips.adapter = tipsAdapter
            rvSingleTags.layoutManager = layoutManagerHorizontal
            rvSingleTags.adapter = RecommendationSingleTagsRecyclerAdapter()
            fabAddTip.setOnClickListener { view ->
                if (!isEditTextTipVisible) {
                    revealEditText(appearedTipLayout)
                    isEditTextTipVisible = true
                    fabAddTip.setImageResource(R.drawable.ic_add_animatable)
                    (fabAddTip.drawable as Animatable).start()
                } else {
                    val newTip = etAppearedTip.text.toString()
                    if (!newTip.equals(""))
                        viewModel.addTip(newTip)
                    etAppearedTip.text.clear()
                    hideEditText(appearedTipLayout)
                    isEditTextTipVisible = false
                    fabAddTip.setImageResource(R.drawable.ic_check_animatable)
                    (fabAddTip.drawable as Animatable).start()
                }
            }
            addTag.setOnClickListener {
                if (!isEditTextTagVisible) {
                    revealEditText(binding.appearedTagLayout)
                    isEditTextTagVisible = true
                    binding.addTag.setImageResource(R.drawable.ic_done_black_24dp)
                } else {
                    val newTag = binding.etAppearedTag.text.toString()
                    if (newTag != "")
                        viewModel.addTag(newTag)
                    binding.etAppearedTag.text.clear()
                    hideEditText(binding.appearedTagLayout)
                    isEditTextTagVisible = false
                }
            }
        }

        viewModel.observeRecommendationSingle()
    }

    override fun onItemClick(item: RecommendationTipViewModel) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun clickOnDelete(v: View) {
        viewModel.deleteRecommendationSingle()
    }
}