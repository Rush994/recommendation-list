package com.smedialink.recommendation_list.presentation.feature.signin

import android.app.Activity
import by.kirich1409.viewbindingdelegate.viewBinding
import com.smedialink.recommendation_list.R
import com.smedialink.recommendation_list.databinding.ActivitySignInBinding
import com.smedialink.recommendation_list.presentation.base.BaseActivity
import com.smedialink.recommendation_list.presentation.base.hideButtonProgressBar
import com.smedialink.recommendation_list.presentation.base.showButtonProgressBar
import com.smedialink.recommendation_list.presentation.base.toast
import com.smedialink.recommendation_list.presentation.custom.progressbutton.attachTextChangeAnimator
import com.smedialink.recommendation_list.presentation.custom.progressbutton.bindProgressButton
import com.smedialink.recommendation_list.presentation.feature.signin.viewmodel.SignInViewModel
import javax.inject.Inject


@Suppress("DEPRECATION")
class SignInActivity : BaseActivity(R.layout.activity_sign_in) {

    private val binding by viewBinding<ActivitySignInBinding>()
    @Inject
    lateinit var viewModel: SignInViewModel

    override fun initListeners() {
        viewModel.states.observe(
            this
        ) { state ->
            state?.let {
                if (it.progressVisible) {
                    showButtonProgressBar(binding.signInButton)
                } else {
                    hideButtonProgressBar(binding.signInButton)
                }
                it.errorText?.let {
                    toast(it)
                }
                if (it.completed) {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }

        }
    }

    override fun initData() {
        with(binding) {
            bindProgressButton(signInButton)
            signInButton.attachTextChangeAnimator()
            signInButton.setOnClickListener {
                viewModel.signIn(
                    username.text.toString(),
                    password.text.toString())
            }
        }
    }
}
