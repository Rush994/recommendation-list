package com.smedialink.recommendation_list.presentation.viewmodel

data class RecommendationTagViewModel(val name: String)