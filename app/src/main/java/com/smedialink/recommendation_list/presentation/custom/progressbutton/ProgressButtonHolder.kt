package com.smedialink.recommendation_list.presentation.custom.progressbutton

import android.animation.Animator
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.material.button.MaterialButton
import java.lang.ref.WeakReference
import java.util.*
import kotlin.collections.ArrayList


internal data class DrawableViewData(var drawable: Drawable, val callback: Drawable.Callback)

internal val attachedViews = WeakHashMap<TextView, TextChangeAnimatorParams>()
internal val activeAnimations = WeakHashMap<TextView, MutableList<Animator>>()
internal val activeViews = WeakHashMap<TextView, DrawableViewData>()

/**
 * Binds your buttons to component lifecycle for the correct data recycling
 * This method is required for all buttons that show progress/drawable
 * @receiver lifecycle owner to which to bin (eg. Activity, Fragment or other)
 * @param button button instance to bind
 */
fun LifecycleOwner.bindProgressButton(button: MaterialButton) {
    lifecycle.addObserver(
        ProgressButtonHolder(
            WeakReference(button)
        )
    )
}

private class ProgressButtonHolder(private val textView: WeakReference<TextView>) : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        textView.get()?.let {
            it.cancelAnimations()
            it.cleanUpDrawable()
            it.removeTextAnimationAttachViewListener()
            it.removeDrawableAttachViewListener()
            attachedViews.remove(it)
        }
    }
}

internal fun TextView.cancelAnimations() {
    if (activeAnimations.containsKey(this)) {
        val animations = activeAnimations[this]!!
        val copy = ArrayList<Animator>(animations)
        copy.forEach {
            it.cancel()
        }
        activeAnimations.remove(this)
    }
}

fun TextView.cleanUpDrawable() {
    if (activeViews.containsKey(this)) {
        activeViews[this]?.drawable?.apply {
            if (this is Animatable) {
                stop()
            }
            callback = null
        }
        activeViews.remove(this)
    }
}

private val textAnimationsAttachListener = object : View.OnAttachStateChangeListener {
    override fun onViewDetachedFromWindow(v: View) {
        if (attachedViews.containsKey(v)) {
            (v as TextView).cancelAnimations()
        }
    }

    override fun onViewAttachedToWindow(v: View?) {
    }
}

internal fun TextView.removeTextAnimationAttachViewListener() {
    removeOnAttachStateChangeListener(textAnimationsAttachListener)
}

private fun TextView.removeDrawableAttachViewListener() {
    removeOnAttachStateChangeListener(drawablesAttachListener)
}

private val drawablesAttachListener = object : View.OnAttachStateChangeListener {
    override fun onViewDetachedFromWindow(v: View?) {
        if (activeViews.containsKey(v)) {
            activeViews[v]?.drawable?.apply {
                if (this is Animatable) {
                    stop()
                }
            }
        }
    }

    override fun onViewAttachedToWindow(v: View?) {
        if (activeViews.containsKey(v)) {
            activeViews[v]?.drawable?.apply {
                if (this is Animatable) {
                    start()
                }
            }
        }
    }
}

internal fun TextView.addTextAnimationAttachViewListener() {
    addOnAttachStateChangeListener(textAnimationsAttachListener)
}

internal fun TextView.addDrawableAttachViewListener() {
    addOnAttachStateChangeListener(drawablesAttachListener)
}