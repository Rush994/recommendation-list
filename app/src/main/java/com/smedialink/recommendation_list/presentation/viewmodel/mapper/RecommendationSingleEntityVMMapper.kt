package com.smedialink.recommendation_list.presentation.viewmodel.mapper

import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationSingleEntity
import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationSingleViewModel
import javax.inject.Inject

class RecommendationSingleEntityVMMapper @Inject constructor(private val recommendationTipEntityVMMapper: RecommendationTipEntityVMMapper,
                                                             private val recommendationTagEntityVMMapper: RecommendationTagEntityVMMapper) : Mapper<RecommendationSingleEntity, RecommendationSingleViewModel>() {
    override fun mapFrom(item: RecommendationSingleEntity): RecommendationSingleViewModel =
        RecommendationSingleViewModel(
            id = item.id,
            isDraft = item.isDraft,
            photo = item.photo,
            category = item.category,
            header = item.header)

    fun mapFrom(list: List<RecommendationSingleEntity>): List<RecommendationSingleViewModel> =
        list.map { mapFrom(it) }
}