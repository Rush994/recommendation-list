package com.smedialink.recommendation_list.presentation.viewmodel

class RecommendationSingleWithTipsViewModel(
    val single: RecommendationSingleViewModel,
    val tips: List<RecommendationTipViewModel>
)