package com.smedialink.recommendation_list.presentation.viewmodel.mapper

import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationTagEntity
import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationTagViewModel
import javax.inject.Inject

class RecommendationTagVMEntityMapper @Inject constructor():
    Mapper<RecommendationTagViewModel, RecommendationTagEntity>() {
    override fun mapFrom(item: RecommendationTagViewModel): RecommendationTagEntity =
        RecommendationTagEntity(name = item.name)

    fun mapFrom(list: List<RecommendationTagViewModel>): List<RecommendationTagEntity> =
        list.map { mapFrom(it) }
}