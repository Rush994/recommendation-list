package com.smedialink.recommendation_list.presentation.viewmodel

data class RecommendationTipViewModel(
    val id: Long = 0,
    val text: String,
    val photo: String? = null
)