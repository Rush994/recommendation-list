package com.smedialink.recommendation_list.presentation.feature.signin.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.smedialink.recommendation_list.di.scope.ViewModelKey
import com.smedialink.recommendation_list.domain.usecase.sign.SignInUseCase
import com.smedialink.recommendation_list.presentation.feature.signin.SignInActivity
import com.smedialink.recommendation_list.presentation.feature.signin.viewmodel.SignInViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class SignInModule {

    @Provides
    @[IntoMap ViewModelKey(SignInViewModel::class)]
    fun provideViewModel(signInUseCase: SignInUseCase): ViewModel =
        SignInViewModel(signInUseCase)

}