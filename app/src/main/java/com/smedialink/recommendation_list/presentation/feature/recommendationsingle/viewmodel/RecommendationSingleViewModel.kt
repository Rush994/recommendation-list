package com.smedialink.recommendation_list.presentation.feature.recommendationsingle.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.smedialink.recommendation_list.domain.usecase.recommendations.*
import com.smedialink.recommendation_list.presentation.base.BaseViewModel
import com.smedialink.recommendation_list.presentation.viewmodel.*
import com.smedialink.recommendation_list.presentation.viewmodel.mapper.RecommendationSingleWithTipsEntityVMMapper
import com.smedialink.recommendation_list.presentation.viewmodel.mapper.RecommendationTagEntityVMMapper
import com.smedialink.recommendation_list.presentation.viewmodel.mapper.RecommendationTagVMEntityMapper
import com.smedialink.recommendation_list.presentation.viewmodel.mapper.RecommendationTipVMEntityMapper
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.HttpException
import javax.inject.Inject

class RecommendationSingleViewModel @Inject constructor(
    private val observeRecommendationSingleWithTipsUseCase: ObserveRecommendationSingleWithTipsUseCase,
    private val observeRecommendationTagsUseCase: ObserveRecommendationTagsUseCase,
    private val addRecommendationTipUseCase: AddRecommendationTipUseCase,
    private val addRecommendationTagUseCase: AddRecommendationTagUseCase,
    private val deleteRecommendationSingleUseCase: DeleteRecommendationSingleUseCase,
    private val recommendationSingleWithTipsEntityVMMapper: RecommendationSingleWithTipsEntityVMMapper,
    private val recommendationTagEntityVMMapper: RecommendationTagEntityVMMapper,
    private val recommendationTipVMEntityMapper: RecommendationTipVMEntityMapper,
    private val recommendationTagVMEntityMapper: RecommendationTagVMEntityMapper
) : BaseViewModel() {


    val recTipLiveDate = MutableLiveData<RecommendationSingleWithTipsViewModel>()
    val tagLiveData = MutableLiveData<List<RecommendationTagViewModel>>()
    val isDeleteLiveData = MutableLiveData<Boolean>()
    var selectedId: Long = 0

    fun observeRecommendationSingle() {
        observeRecommendationSingleWithTipsUseCase(selectedId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map {
                recommendationSingleWithTipsEntityVMMapper.mapFrom(it)
            }
            .subscribe(
                {
                    recTipLiveDate.value = it
                },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                }
            )
            .also { disposables.add(it) }

        observeRecommendationTagsUseCase(selectedId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map {
                recommendationTagEntityVMMapper.mapFrom(it)
            }
            .subscribe(
                {
                    tagLiveData.value = it
                },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                }
            )
            .also { disposables.add(it) }
    }

    fun addTip(tipName: String) {
        val listTip: List<RecommendationTipViewModel> =
            listOf(RecommendationTipViewModel(text = tipName))
        addRecommendationTipUseCase(
            recTipLiveDate.value?.single!!.id,
            recommendationTipVMEntityMapper.mapFrom(listTip)
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                })
            .also { disposables.add(it) }

    }

    fun addTag(tagName: String) {
        val listTag: MutableList<RecommendationTagViewModel> = tagLiveData.value!!.toMutableList()
        listTag.add(RecommendationTagViewModel(name = tagName))
        addRecommendationTagUseCase(
            recTipLiveDate.value?.single!!.id,
            recommendationTagVMEntityMapper.mapFrom(RecommendationTagViewModel(name = tagName)),
            recommendationTagVMEntityMapper.mapFrom(listTag)
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                })
            .also { disposables.add(it) }

    }

    fun deleteRecommendationSingle() {
        deleteRecommendationSingleUseCase(recTipLiveDate.value!!.single.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                isDeleteLiveData.value = true
                Log.d("Recommendation info", "Recommendotion deleted")
            }
            .also { disposables.add(it) }
    }
}