package com.smedialink.recommendation_list.presentation.feature.signup

import android.app.Activity
import by.kirich1409.viewbindingdelegate.viewBinding
import com.smedialink.recommendation_list.R
import com.smedialink.recommendation_list.databinding.ActivitySignUpBinding
import com.smedialink.recommendation_list.presentation.base.BaseActivity
import com.smedialink.recommendation_list.presentation.base.hideButtonProgressBar
import com.smedialink.recommendation_list.presentation.base.showButtonProgressBar
import com.smedialink.recommendation_list.presentation.base.toast
import com.smedialink.recommendation_list.presentation.custom.progressbutton.attachTextChangeAnimator
import com.smedialink.recommendation_list.presentation.custom.progressbutton.bindProgressButton
import com.smedialink.recommendation_list.presentation.feature.signup.viewmodel.SignUpViewModel
import java.util.regex.Pattern
import javax.inject.Inject

class SignUpActivity : BaseActivity(R.layout.activity_sign_up) {

    private val binding by viewBinding<ActivitySignUpBinding>()

    @Inject
    lateinit var viewModel: SignUpViewModel

    override fun initListeners() {
        viewModel.states.observe(
            this
        ) { state ->
            state?.let {
                if (it.progressVisible) {
                    showButtonProgressBar(binding.signUpButton)
                } else {
                    hideButtonProgressBar(binding.signUpButton)
                }
                if (it.completed) {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }

        }
        viewModel.usernameErrors.observe(this) { state ->
            state.let {
                binding.tilUserName.error = it.textInputLayoutErrorMessage
            }
        }
        viewModel.toastErrors.observe(this) { state ->
            toast(state.textInputLayoutErrorMessage)
        }
    }

    override fun initData() {
        with(binding) {
            bindProgressButton(signUpButton)
            signUpButton.attachTextChangeAnimator()

            signUpButton.setOnClickListener {
                var isEmptyField = false
                val sFirstName: String = firstName.text.toString()
                val sLastName: String = lastName.text.toString()
                val sUserName:String = userName.text.toString()
                val sEmail: String = email.text.toString()
                val sPassword: String = password.text.toString()

                if(sFirstName == "") {
                    tilFirstName.error = resources.getString(R.string.not_empty, "FrstName")
                    isEmptyField = true
                }
                else
                    tilFirstName.error = null

                if(sLastName == "") {
                    tilLastName.error = resources.getString(R.string.not_empty, "LastName")
                    isEmptyField = true
                }
                else
                    tilLastName.error = null

                /// UserName validation
                if(sUserName == "") {
                    tilUserName.error = resources.getString(R.string.not_empty, "UserName")
                    isEmptyField = true
                }
                else
                    tilUserName.error = null

                /// Email validation
                if(sEmail.equals("")) {
                    tilEmail.error = resources.getString(R.string.not_empty, "Email")
                    isEmptyField = true
                }
                else
                    tilEmail.error = null
                val EMAIL_ADDRESS_PATTERN = Pattern
                    .compile("[a-zA-Z0-9+._%-+]{1,256}" + "@"
                            + "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" + "(" + "."
                            + "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" + ")+")
                if(!EMAIL_ADDRESS_PATTERN.matcher(sEmail).matches())
                    tilEmail.error = "Valid email id is required"
                else
                    tilEmail.error = null

                /// Password validation
                if(sPassword.equals("")) {
                    tilPassword.error = resources.getString(R.string.not_empty, "Password")
                    isEmptyField = true
                }
                else if (sPassword.equals("") || sPassword.length < 8) {
                    tilPassword.error = "Password length should be grater then 8 symbols"
                    isEmptyField = true
                }
                else
                    tilPassword.error = null

                if (!isEmptyField)
                    viewModel.signUp(firstName.text.toString(), lastName.text.toString(), userName.text.toString(), email.text.toString(), password.text.toString(), true)
            }
        }
    }
}
