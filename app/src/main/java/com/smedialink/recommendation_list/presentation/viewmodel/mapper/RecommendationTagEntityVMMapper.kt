package com.smedialink.recommendation_list.presentation.viewmodel.mapper

import com.smedialink.recommendation_list.data.network.response.RecommendationTipResp
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationTagEntity
import com.smedialink.recommendation_list.domain.entity.RecommendationTipEntity
import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationTagViewModel
import javax.inject.Inject

class RecommendationTagEntityVMMapper @Inject constructor() :
    Mapper<RecommendationTagEntity, RecommendationTagViewModel>() {
    override fun mapFrom(item: RecommendationTagEntity): RecommendationTagViewModel =
        RecommendationTagViewModel(item.name)

    fun mapFrom(list: List<RecommendationTagEntity>): List<RecommendationTagViewModel> =
        list.map { mapFrom(it) }
}