package com.smedialink.recommendation_list.presentation.feature.recommendationlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.smedialink.recommendation_list.databinding.RecyclerviewRecommendationItemBinding
import com.smedialink.recommendation_list.presentation.base.interfaces.OnNameItemClickListener
import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationSingleViewModel

class RecommendationListNamesRecyclerAdapter() :
    RecyclerView.Adapter<RecommendationListNamesRecyclerAdapter.SingleItemHolder>() {

    var listener: OnNameItemClickListener? = null
    private var recommendations: List<RecommendationSingleViewModel> = mutableListOf()

    fun setRecommendationList(recommendations: List<RecommendationSingleViewModel>) {
        this.recommendations = recommendations
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SingleItemHolder(
        RecyclerviewRecommendationItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount(): Int = recommendations.size

    override fun onBindViewHolder(holder: SingleItemHolder, position: Int) {
        val item = recommendations[position]
        holder.bind(item)
    }

    fun setOnNameClickListener(listener: OnNameItemClickListener) {
        this.listener = listener
    }

    inner class SingleItemHolder(private val itemBinding: RecyclerviewRecommendationItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(item: RecommendationSingleViewModel) {
            with(itemBinding) {
                tvItemName.text = item.header
                root.setOnClickListener {
                    listener?.onItemClick(item)
                }
            }
        }
    }
}