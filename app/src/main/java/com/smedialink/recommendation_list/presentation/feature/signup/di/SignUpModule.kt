package com.smedialink.recommendation_list.presentation.feature.signup.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.smedialink.recommendation_list.di.scope.ViewModelKey
import com.smedialink.recommendation_list.domain.usecase.sign.SignUpUseCase
import com.smedialink.recommendation_list.presentation.feature.signup.SignUpActivity
import com.smedialink.recommendation_list.presentation.feature.signup.viewmodel.SignUpViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class SignUpModule {

    @Provides
    @[IntoMap ViewModelKey(SignUpViewModel::class)]
    fun provideViewModel(signUpUseCase: SignUpUseCase): ViewModel =
        SignUpViewModel(signUpUseCase)
}