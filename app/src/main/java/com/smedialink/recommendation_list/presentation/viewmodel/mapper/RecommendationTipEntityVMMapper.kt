package com.smedialink.recommendation_list.presentation.viewmodel.mapper

import com.smedialink.recommendation_list.data.network.response.RecommendationTipResp
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationTipEntity
import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationTipViewModel
import javax.inject.Inject

class RecommendationTipEntityVMMapper @Inject constructor() :
    Mapper<RecommendationTipEntity, RecommendationTipViewModel>() {
    override fun mapFrom(item: RecommendationTipEntity): RecommendationTipViewModel =
        RecommendationTipViewModel(
            id = item.id,
            text = item.text,
            photo = item.photo)

    fun mapFrom(list: List<RecommendationTipEntity>): List<RecommendationTipViewModel> =
        list.map{ mapFrom(it) }

}