package com.smedialink.recommendation_list.presentation.viewmodel

data class UserProfileViewModel(
    val id: Long,
    val email: String,
    val firstName: String,
    val lastName: String,
    val userName: String,
    val isActive: Boolean,
    val avatar: String?
)