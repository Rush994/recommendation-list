package com.smedialink.recommendation_list.presentation.feature.sign.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.smedialink.recommendation_list.domain.usecase.sign.SignedInCheckUseCase
import com.smedialink.recommendation_list.presentation.base.BaseViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class SignViewModel @Inject constructor(val signedInCheckUseCase: SignedInCheckUseCase): BaseViewModel()
{
    val firingActivity = MutableLiveData<Boolean>()
    val loggedOut = MutableLiveData<Boolean>()

    fun signedInCheck(){
        signedInCheckUseCase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( { item ->
                firingActivity.value = item
                if(!item) {
                    loggedOut.value = false
                }}, {
                Log.d("Error", it.toString())
            })
            .also { disposables.add(it) }
    }
}