package com.smedialink.recommendation_list.presentation.feature.recommendationsingle.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.smedialink.recommendation_list.databinding.ReciclerviewRecommendationTipItemBinding
import com.smedialink.recommendation_list.presentation.base.interfaces.OnTipItemClickListener
import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationTipViewModel

class RecommendationSingleTipsRecyclerAdapter() :
    RecyclerView.Adapter<RecommendationSingleTipsRecyclerAdapter.SingleItemHolder>() {

    var listener: OnTipItemClickListener? = null
    private var tips: List<RecommendationTipViewModel> = mutableListOf()

    fun setTipList(tips: List<RecommendationTipViewModel>) {
        this.tips = tips
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SingleItemHolder = SingleItemHolder(
        ReciclerviewRecommendationTipItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount(): Int = tips.size

    override fun onBindViewHolder(holder: SingleItemHolder, position: Int) {
        val item = tips[position]
        holder.bind(item, position)
    }

    fun setOnTipClickListener(listener: OnTipItemClickListener) {
        this.listener = listener
    }

    inner class SingleItemHolder(private val itemBinding: ReciclerviewRecommendationTipItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(item: RecommendationTipViewModel, position: Int) {
            with(itemBinding) {
                var currentNum = position
                tvItemTipHeaderValue.text = (++currentNum).toString()
                tvItemTipText.text = item.text

                root.setOnClickListener {
                    listener?.onItemClick(item)
                }
            }
        }
    }
}