package com.smedialink.recommendation_list.presentation.feature.recommendationlist.di

import androidx.lifecycle.ViewModel
import com.smedialink.recommendation_list.di.scope.ViewModelKey
import com.smedialink.recommendation_list.domain.usecase.profile.ObserveProfileInfoUseCase
import com.smedialink.recommendation_list.domain.usecase.recommendations.LoadUserRecommendationListUseCase
import com.smedialink.recommendation_list.domain.usecase.recommendations.ObserveRecommendationSingleListUseCase
import com.smedialink.recommendation_list.domain.usecase.recommendations.PostUserRecommendationSingleUseCase
import com.smedialink.recommendation_list.domain.usecase.sign.SignOutUseCase
import com.smedialink.recommendation_list.presentation.feature.recommendationlist.viewmodel.RecommendationListViewModel
import com.smedialink.recommendation_list.presentation.viewmodel.mapper.RecommendationSingleEntityVMMapper
import com.smedialink.recommendation_list.presentation.viewmodel.mapper.UserProfileEntityVMMapper
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class RecommendationListModule {
    @Provides
    @[IntoMap ViewModelKey(RecommendationListViewModel::class)]
    fun provideViewModel(
        observeProfileInfoUseCase: ObserveProfileInfoUseCase,
        loadUserRecommendationListUseCase: LoadUserRecommendationListUseCase,
        observeRecommendationSingleListUseCase: ObserveRecommendationSingleListUseCase,
        postUserRecommendationSingleUseCase: PostUserRecommendationSingleUseCase,
        signOutUseCase: SignOutUseCase,
        userProfileEntityVMMapper: UserProfileEntityVMMapper,
        recommendationSingleEntityVMMapper: RecommendationSingleEntityVMMapper
    ): ViewModel =
        RecommendationListViewModel(
            observeProfileInfoUseCase,
            loadUserRecommendationListUseCase,
            observeRecommendationSingleListUseCase,
            postUserRecommendationSingleUseCase,
            signOutUseCase,
            userProfileEntityVMMapper,
            recommendationSingleEntityVMMapper
        )
}