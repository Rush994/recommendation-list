package com.smedialink.recommendation_list.presentation.base.enum

enum class PageTypes(val value: Int) {
    None(0), Sign(1), SignIn(2),
    SignUp(3), RecommendationList(4),
    RecommendationSingle(5)
}