package com.smedialink.recommendation_list.presentation.feature.recommendationlist

import android.graphics.drawable.Animatable
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.smedialink.recommendation_list.R
import com.smedialink.recommendation_list.databinding.ActivityRecommendationListBinding
import com.smedialink.recommendation_list.presentation.base.BaseActivity
import com.smedialink.recommendation_list.presentation.base.enum.PageTypes
import com.smedialink.recommendation_list.presentation.base.hideEditText
import com.smedialink.recommendation_list.presentation.base.interfaces.OnNameItemClickListener
import com.smedialink.recommendation_list.presentation.base.revealEditText
import com.smedialink.recommendation_list.presentation.feature.recommendationlist.adapter.RecommendationListNamesRecyclerAdapter
import com.smedialink.recommendation_list.presentation.feature.recommendationlist.viewmodel.RecommendationListViewModel
import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationSingleViewModel
import javax.inject.Inject


class RecommendationListActivity : BaseActivity(R.layout.activity_recommendation_list),
    OnNameItemClickListener {

    private val binding by viewBinding<ActivityRecommendationListBinding>()

    @Inject
    lateinit var viewModel: RecommendationListViewModel

    private var layoutManagerVertical = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
    private lateinit var recommendAdapter: RecommendationListNamesRecyclerAdapter
    private var isEditTextVisible: Boolean = false

    override fun initListeners() {
        viewModel.isSignedOutLiveData.observe(
            this
        ) { state ->
            if (state) {
                extinguishActivity(this)
                fireActivity(this, PageTypes.Sign)
            }
        }
        viewModel.userProfileLiveData.observe(
            this
        ) { state ->
            state.let {
                binding.tvUsername.text = it.userName
            }
        }
        viewModel.userRecommendationSingleListLiveData.observe(
            this
        ) { list ->
            recommendAdapter.setRecommendationList(list)
        }
    }

    override fun initData() {
        recommendAdapter = RecommendationListNamesRecyclerAdapter()
        recommendAdapter.setOnNameClickListener(this)
        val dividerItemDecoration = DividerItemDecoration(
            this, layoutManagerVertical.orientation
        )
        with(binding) {
            rvMain.layoutManager = layoutManagerVertical
            rvMain.addItemDecoration(dividerItemDecoration)
            rvMain.adapter = recommendAdapter
            fabAddRecommendation.setOnClickListener { view ->
                if (!isEditTextVisible) {
                    revealEditText(appearedRecLayout)
                    isEditTextVisible = true
                    fabAddRecommendation.setImageResource(R.drawable.ic_add_to_done)
                    (fabAddRecommendation.drawable as Animatable).start()
                } else {
                    val newRecommendation = etAppearedRec.text.toString()
                    if (newRecommendation != "")
                        viewModel.createRecommendation(newRecommendation)
                    etAppearedRec.text.clear()
                    hideEditText(appearedRecLayout)
                    isEditTextVisible = false
                    fabAddRecommendation.setImageResource(R.drawable.ic_done_to_add)
                    (fabAddRecommendation.drawable as Animatable).start()
                }
            }
        }

        viewModel.observeProfileInfo()
        viewModel.observeUserRecommendations()
    }

    override fun onItemClick(item: RecommendationSingleViewModel) {
        viewModel.selectedItem = item
        fireActivity(this, PageTypes.RecommendationSingle, item.id)
    }
}