package com.smedialink.recommendation_list.presentation.viewmodel.mapper

import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationSingleWithTipsEntity
import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationSingleWithTipsViewModel
import javax.inject.Inject

class RecommendationSingleWithTipsEntityVMMapper @Inject constructor (
    private val recommendationSingleEntityVMMapper: RecommendationSingleEntityVMMapper,
    private val recommendationTipEntityVMMapper: RecommendationTipEntityVMMapper
): Mapper<RecommendationSingleWithTipsEntity, RecommendationSingleWithTipsViewModel>(){

    override fun mapFrom(item: RecommendationSingleWithTipsEntity): RecommendationSingleWithTipsViewModel =
        RecommendationSingleWithTipsViewModel(
            recommendationSingleEntityVMMapper.mapFrom(item.single),
            recommendationTipEntityVMMapper.mapFrom(item.tips))

    fun mapFrom(list: List<RecommendationSingleWithTipsEntity>): List<RecommendationSingleWithTipsViewModel> =
        list.map { mapFrom(it) }

}