package com.smedialink.recommendation_list.presentation.base

import dagger.android.support.DaggerFragment

open class BaseFragment: DaggerFragment() {
}