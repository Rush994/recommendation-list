package com.smedialink.recommendation_list.presentation.base.interfaces

import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationTipViewModel

interface OnTipItemClickListener {
    fun onItemClick(item: RecommendationTipViewModel)
}