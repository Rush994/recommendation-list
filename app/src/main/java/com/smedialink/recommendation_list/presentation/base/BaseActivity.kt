package com.smedialink.recommendation_list.presentation.base

import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.smedialink.recommendation_list.R
import com.smedialink.recommendation_list.presentation.navigation.NavigationManager
import com.smedialink.recommendation_list.presentation.base.enum.PageTypes
import dagger.android.support.DaggerAppCompatActivity
import java.util.ArrayList

abstract class BaseActivity(
    @LayoutRes private val layoutId: Int): DaggerAppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        initListeners()
        initData()
    }

    override fun onBackPressed() {
        extinguishActivity(this)
    }

    fun fireActivity(activity: AppCompatActivity, destination: PageTypes, recommendationId: Long = 0) {
        NavigationManager.getInstance().Navigate(destination, activity, recommendationId)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    fun extinguishActivity(activity: AppCompatActivity){
        activity.finish()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    abstract fun initListeners()
    abstract fun initData()
}