package com.smedialink.recommendation_list.presentation.feature.sign.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.smedialink.recommendation_list.di.scope.ViewModelKey
import com.smedialink.recommendation_list.domain.usecase.sign.SignedInCheckUseCase
import com.smedialink.recommendation_list.presentation.feature.sign.SignActivity
import com.smedialink.recommendation_list.presentation.feature.sign.viewmodel.SignViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class SignModule {

    @Provides
    @[IntoMap ViewModelKey(SignViewModel::class)]
    fun provideViewModel(signedInCheckUseCase: SignedInCheckUseCase): ViewModel =
        SignViewModel(signedInCheckUseCase)
}