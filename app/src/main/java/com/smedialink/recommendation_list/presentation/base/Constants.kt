package com.smedialink.recommendation_list.presentation.base

class Constants{
    companion object{
        const val RESULT_OK = 10
        const val RESULT_FAILURE = 5

        const val IS_FURTHER = "CanGoFurther"
        const val ERROR_TEXT = "ErrorText"

        const val TAG_AUTHORIZE = "Authorization: "

        const val TAG_LIVEDATA = "Value from LiveData"

        const val SIGNED_RESULT = 10

        const val RECOMMENDATION_ID = "RecommendationId"
    }

}