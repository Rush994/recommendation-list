package com.smedialink.recommendation_list.presentation.base

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.graphics.Color
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewAnimationUtils
import android.widget.Button
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import com.smedialink.recommendation_list.R
import com.smedialink.recommendation_list.presentation.custom.progressbutton.hideProgress
import com.smedialink.recommendation_list.presentation.custom.progressbutton.showProgress

////////////////////////////////////////// Toast extension method
fun Context.toast(message: String) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
/////////////////////////////////////////////////////////////////

// logger
fun Context.logging(message: String, tag: String) =
    Log.d(tag, message)
///////////

//region ProgressButton handling
fun Context.showButtonProgressBar(button: Button) {
    button.showProgress {
        buttonTextRes = R.string.action_sign_up_loading
        progressColor = Color.WHITE
    }
    button.isEnabled = false
}

fun Context.hideButtonProgressBar(button: Button){
    button.isEnabled = true
    button.hideProgress(R.string.action_sign_up)
}
//endregion

//region Animation handling
fun Context.revealEditText(view: ConstraintLayout) {
    val cx = view.right - 30
    val cy = view.bottom - 60
    val finalRadius = Math.max(view.width, view.height)
    val anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0f, finalRadius.toFloat())
    view.visibility = View.VISIBLE
    anim.start()
}

fun Context.hideEditText(view: ConstraintLayout) {
    val cx = view.right - 30
    val cy = view.bottom - 60
    val initialRadius = view.width
    val anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius.toFloat(), 0f)
    anim.addListener(object : AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator) {
            super.onAnimationEnd(animation)
            view.visibility = View.INVISIBLE
        }
    })
    anim.start()
}
//endregion