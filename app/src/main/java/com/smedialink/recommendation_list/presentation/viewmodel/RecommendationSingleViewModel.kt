package com.smedialink.recommendation_list.presentation.viewmodel

import com.smedialink.recommendation_list.domain.entity.RecommendationTagEntity
import com.smedialink.recommendation_list.domain.entity.RecommendationTipEntity

data class RecommendationSingleViewModel(
    val id: Long,
    val isDraft: Boolean,
    val photo: String?,
    val category: String,
    val header: String
)