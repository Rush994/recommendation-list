package com.smedialink.recommendation_list.presentation.feature.recommendationsingle.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.smedialink.recommendation_list.di.scope.ViewModelKey
import com.smedialink.recommendation_list.domain.usecase.recommendations.AddRecommendationTagUseCase
import com.smedialink.recommendation_list.domain.usecase.recommendations.AddRecommendationTipUseCase
import com.smedialink.recommendation_list.domain.usecase.recommendations.DeleteRecommendationSingleUseCase
import com.smedialink.recommendation_list.domain.usecase.recommendations.ObserveRecommendationSingleWithTipsUseCase
import com.smedialink.recommendation_list.domain.usecase.recommendations.ObserveRecommendationTagsUseCase
import com.smedialink.recommendation_list.presentation.feature.recommendationsingle.RecommendationSingleActivity
import com.smedialink.recommendation_list.presentation.feature.recommendationsingle.viewmodel.RecommendationSingleViewModel
import com.smedialink.recommendation_list.presentation.viewmodel.mapper.RecommendationSingleWithTipsEntityVMMapper
import com.smedialink.recommendation_list.presentation.viewmodel.mapper.RecommendationTagEntityVMMapper
import com.smedialink.recommendation_list.presentation.viewmodel.mapper.RecommendationTagVMEntityMapper
import com.smedialink.recommendation_list.presentation.viewmodel.mapper.RecommendationTipVMEntityMapper
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class RecommendationSingleModule {
    @Provides
    @[IntoMap ViewModelKey(RecommendationSingleViewModel::class)]
    fun provideViewModel(
        observeRecommendationSingleWithTipsUseCase: ObserveRecommendationSingleWithTipsUseCase,
        observeRecommendationTagsUseCase: ObserveRecommendationTagsUseCase,
        addRecommendationTipUseCase: AddRecommendationTipUseCase,
        addRecommendationTagUseCase: AddRecommendationTagUseCase,
        deleteRecommendationSingleUseCase: DeleteRecommendationSingleUseCase,
        recommendationSingleWithTagsEntityVMMapper: RecommendationSingleWithTipsEntityVMMapper,
        recommendationTagEntityVMMapper: RecommendationTagEntityVMMapper,
        recommendationTipVMEntityMapper: RecommendationTipVMEntityMapper,
        recommendationTagVMEntityMapper: RecommendationTagVMEntityMapper
    ): ViewModel =
        RecommendationSingleViewModel(
            observeRecommendationSingleWithTipsUseCase,
            observeRecommendationTagsUseCase, addRecommendationTipUseCase,
            addRecommendationTagUseCase, deleteRecommendationSingleUseCase,
            recommendationSingleWithTagsEntityVMMapper, recommendationTagEntityVMMapper,
            recommendationTipVMEntityMapper, recommendationTagVMEntityMapper
        )
}