package com.smedialink.recommendation_list.presentation.viewmodel.mapper

import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.UserProfileEntity
import com.smedialink.recommendation_list.presentation.viewmodel.UserProfileViewModel
import javax.inject.Inject

class UserProfileEntityVMMapper @Inject constructor(): Mapper<UserProfileEntity, UserProfileViewModel>() {
    override fun mapFrom(item: UserProfileEntity): UserProfileViewModel =
        UserProfileViewModel(
            id = item.id, 
            email = item.email, 
            firstName = item.firstName, 
            lastName = item.lastName,
            userName = item.userName, 
            isActive = item.isActive, 
            avatar = item.avatar)
}