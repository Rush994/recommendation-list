package com.smedialink.recommendation_list.presentation.feature.sign

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.smedialink.recommendation_list.R
import com.smedialink.recommendation_list.presentation.base.*
import com.smedialink.recommendation_list.presentation.base.enum.PageTypes
import com.smedialink.recommendation_list.presentation.feature.sign.viewmodel.SignViewModel
import com.smedialink.recommendation_list.presentation.feature.signin.SignInActivity
import com.smedialink.recommendation_list.presentation.feature.signup.SignUpActivity
import javax.inject.Inject

class SignActivity : BaseActivity(R.layout.activity_sign) {

    @Inject
    lateinit var viewModel: SignViewModel

    private var snackBar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_NoActionBar)
        super.onCreate(savedInstanceState)
    }

    override fun initListeners() {
        viewModel.firingActivity.observe(
            this,
            Observer { state ->
                state?.let {
                    if (it) {
                        fireActivity(this, PageTypes.RecommendationList)
                        finish()
                    } else{
                        toast("User should log in")
                    }
                }

            })
    }

    override fun initData() {
        viewModel.signedInCheck()
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.SIGNED_RESULT && resultCode == Activity.RESULT_OK) {
            viewModel.signedInCheck()
        }
    }

    fun loginPressed(v: View){
        intent = Intent(this, SignInActivity::class.java)
        startActivityForResult(intent, Constants.SIGNED_RESULT)
    }

    fun registrationPressed(v: View){
        intent = Intent(this, SignUpActivity::class.java)
        startActivityForResult(intent, Constants.SIGNED_RESULT)
    }
}
