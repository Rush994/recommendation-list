package com.smedialink.recommendation_list.presentation.feature.signup.viewmodel

import androidx.lifecycle.MutableLiveData
import com.smedialink.recommendation_list.domain.usecase.sign.SignUpUseCase
import com.smedialink.recommendation_list.presentation.base.BaseViewModel
import com.smedialink.recommendation_list.presentation.base.`object`.ErrorMessage
import com.smedialink.recommendation_list.presentation.base.`object`.SignState
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.HttpException
import javax.inject.Inject

class SignUpViewModel @Inject constructor(private val signUpUseCase: SignUpUseCase) : BaseViewModel() {

    val states: MutableLiveData<SignState> = MutableLiveData()
    val usernameErrors: MutableLiveData<ErrorMessage> = MutableLiveData()
    val toastErrors: MutableLiveData<ErrorMessage> = MutableLiveData()
    val currentState = SignState()

    fun signUp(
        firstName: String, lastName: String,
        userName: String, email: String, password: String,
        isActive: Boolean
    ) =
        signUpUseCase(firstName, lastName, userName, email, password, isActive)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                states.value = currentState.apply {
                    progressVisible = true
                    completed = false
                }
            }
            .subscribe(
                {
                    states.value = currentState.apply {
                        progressVisible = false
                        completed = true
                    }
                }, {
                    states.value = currentState.apply {
                        progressVisible = false
                        errorText = it.message
                    }
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                        val json = JSONObject(resp?.string().orEmpty())
                        val jsonUserName: String = json.getString("username")
                        if (jsonUserName != "")
                            usernameErrors.value = ErrorMessage(jsonUserName.substring(2, jsonUserName.length - 2))
                        else
                            toastErrors.value = ErrorMessage(json.toString())
                        //Log.d(Constants.TAG_AUTHORIZE, JSONObject(resp?.string()).toString())
                    }
                    //errorJson.getString("userName")
                }).also { disposables.add(it) }
}