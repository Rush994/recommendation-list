package com.smedialink.recommendation_list.presentation.feature.signin.viewmodel

import androidx.lifecycle.MutableLiveData
import com.smedialink.recommendation_list.domain.usecase.sign.SignInUseCase
import com.smedialink.recommendation_list.presentation.base.BaseViewModel
import com.smedialink.recommendation_list.presentation.base.`object`.SignState
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class SignInViewModel @Inject constructor(val signInUseCase: SignInUseCase): BaseViewModel() {

    val states: MutableLiveData<SignState> = MutableLiveData<SignState>()
    val currentState = SignState()


    fun signIn(username: String, password: String) {

        signInUseCase(username, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    states.value = currentState.apply {
                        progressVisible = false
                        completed = true
                    }
                }, {
                    states.value = currentState.apply {
                        progressVisible = false
                        errorText = it.message
                    }
                }).also { disposables.add(it) }
    }
}