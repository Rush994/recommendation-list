package com.smedialink.recommendation_list.presentation.feature.recommendationlist.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.smedialink.recommendation_list.domain.entity.RecommendationSingleEntity
import com.smedialink.recommendation_list.domain.usecase.profile.ObserveProfileInfoUseCase
import com.smedialink.recommendation_list.domain.usecase.recommendations.LoadUserRecommendationListUseCase
import com.smedialink.recommendation_list.domain.usecase.recommendations.ObserveRecommendationSingleListUseCase
import com.smedialink.recommendation_list.domain.usecase.recommendations.PostUserRecommendationSingleUseCase
import com.smedialink.recommendation_list.domain.usecase.sign.SignOutUseCase
import com.smedialink.recommendation_list.presentation.base.BaseViewModel
import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationSingleViewModel
import com.smedialink.recommendation_list.presentation.viewmodel.UserProfileViewModel
import com.smedialink.recommendation_list.presentation.viewmodel.mapper.RecommendationSingleEntityVMMapper
import com.smedialink.recommendation_list.presentation.viewmodel.mapper.UserProfileEntityVMMapper
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.HttpException
import javax.inject.Inject

class RecommendationListViewModel @Inject constructor(
    private val observeProfileInfoUseCase: ObserveProfileInfoUseCase,
    private val loadUserRecommendationListUseCase: LoadUserRecommendationListUseCase,
    private val observeRecommendationSingleListUseCase: ObserveRecommendationSingleListUseCase,
    private val postUserRecommendationSingleUseCase: PostUserRecommendationSingleUseCase,
    private val signOutUseCase: SignOutUseCase,
    private val userProfileEntityVMMapper: UserProfileEntityVMMapper,
    private val recommendationSingleEntityVMMapper: RecommendationSingleEntityVMMapper
): BaseViewModel() {

    val userProfileLiveData = MutableLiveData<UserProfileViewModel>()
    val userRecommendationSingleListLiveData = MutableLiveData<List<RecommendationSingleViewModel>>()
    val isSignedOutLiveData = MutableLiveData<Boolean>()

    var selectedItem: RecommendationSingleViewModel? = null
        set(value){
            field = value
        }

    fun observeProfileInfo(){
        observeProfileInfoUseCase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { userProfileEntityVMMapper.mapFrom(it) }
            .subscribe ({ resp ->
                userProfileLiveData.value = resp
            },{
                if (it is HttpException) {
                    val resp = it.response()?.errorBody()
                }
                Log.d("Not get: ", it.toString())
            })
            .also { disposables.add(it)  }
    }

    fun observeUserRecommendations(){
//        loadUserRecommendationListUseCase()
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe({},
//                {
//                    if (it is HttpException) {
//                        val resp = it.response()?.errorBody()
//                    }
//                    Log.d("Not get: ", it.toString())
//                }).also { disposables.add(it) }

        observeRecommendationSingleListUseCase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { recommendationSingleEntityVMMapper.mapFrom(it) }
            .subscribe({
                userRecommendationSingleListLiveData.value = it
            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                })
            .also { disposables.add(it) }
    }

    fun createRecommendation(header: String){
        postUserRecommendationSingleUseCase(RecommendationSingleEntity(header = header))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                })
            .also { disposables.add(it) }
    }

    fun signOut()
    {
        signOutUseCase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                isSignedOutLiveData.value = true
            }.also { disposables.add(it) }

    }

}