package com.smedialink.recommendation_list.presentation.base.interfaces

import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationSingleViewModel

interface OnNameItemClickListener {
    fun onItemClick(item: RecommendationSingleViewModel)
}