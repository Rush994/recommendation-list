package com.smedialink.recommendation_list.presentation.base.`object`

data class SignState(
    var progressVisible: Boolean = false,
    var errorText: String? = null,
    var completed: Boolean = false
)
