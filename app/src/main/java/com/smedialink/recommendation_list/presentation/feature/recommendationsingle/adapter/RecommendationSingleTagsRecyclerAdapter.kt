package com.smedialink.recommendation_list.presentation.feature.recommendationsingle.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.smedialink.recommendation_list.databinding.RecyclerviewRecommendationTagItemBinding
import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationTagViewModel

class RecommendationSingleTagsRecyclerAdapter() :
    RecyclerView.Adapter<RecommendationSingleTagsRecyclerAdapter.SingleItemHolder>() {

    private var tags: List<RecommendationTagViewModel> = listOf()

    fun setTipList(tags: List<RecommendationTagViewModel>) {
        this.tags = tags
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecommendationSingleTagsRecyclerAdapter.SingleItemHolder = SingleItemHolder(
        RecyclerviewRecommendationTagItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount(): Int = tags.size

    override fun onBindViewHolder(
        holder: RecommendationSingleTagsRecyclerAdapter.SingleItemHolder,
        position: Int
    ) {
        val item = tags[position]
        holder.bind(item)
    }

    inner class SingleItemHolder(private val itemBinding: RecyclerviewRecommendationTagItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(item: RecommendationTagViewModel) {
            with(itemBinding) {
                chipTagItem.text = item.name
            }
        }
    }
}