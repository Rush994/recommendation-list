package com.smedialink.recommendation_list.presentation.navigation

import android.content.Context
import android.content.Intent
import com.smedialink.recommendation_list.presentation.base.Constants
import com.smedialink.recommendation_list.presentation.base.enum.PageTypes
import com.smedialink.recommendation_list.presentation.feature.recommendationlist.RecommendationListActivity
import com.smedialink.recommendation_list.presentation.feature.recommendationsingle.RecommendationSingleActivity
import com.smedialink.recommendation_list.presentation.feature.sign.SignActivity
import com.smedialink.recommendation_list.presentation.feature.signin.SignInActivity
import com.smedialink.recommendation_list.presentation.feature.signup.SignUpActivity


class NavigationManager {

    companion object{
        private val instance: NavigationManager =
            NavigationManager()
        fun getInstance() : NavigationManager =
            instance
    }

    var currentPage = PageTypes.None

    private var pageContext: Context? = null

    fun Navigate(page: PageTypes, context: Context, recommendationId: Long) {
        pageContext = context
        navigate(page, recommendationId)
    }

    private fun navigate(page: PageTypes, recommendationId: Long) {
        currentPage = page
        when (page) {
            PageTypes.Sign -> pageContext?.startActivity(Intent(pageContext, SignActivity::class.java))
            PageTypes.SignIn -> pageContext?.startActivity(Intent(pageContext, SignInActivity::class.java))
            PageTypes.SignUp -> pageContext?.startActivity(Intent(pageContext, SignUpActivity::class.java))
            PageTypes.RecommendationList -> pageContext?.startActivity(Intent(pageContext, RecommendationListActivity::class.java))
            PageTypes.RecommendationSingle -> pageContext?.startActivity(Intent(pageContext, RecommendationSingleActivity::class.java).putExtra(Constants.RECOMMENDATION_ID, recommendationId))
            else -> pageContext?.startActivity(Intent(pageContext, SignActivity::class.java))
        }
    }
}