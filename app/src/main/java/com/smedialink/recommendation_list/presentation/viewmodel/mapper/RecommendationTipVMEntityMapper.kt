package com.smedialink.recommendation_list.presentation.viewmodel.mapper

import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationTipEntity
import com.smedialink.recommendation_list.presentation.viewmodel.RecommendationTipViewModel
import javax.inject.Inject

class RecommendationTipVMEntityMapper @Inject constructor() :
    Mapper<RecommendationTipViewModel, RecommendationTipEntity>() {
    override fun mapFrom(item: RecommendationTipViewModel): RecommendationTipEntity =
        RecommendationTipEntity(
            text = item.text)

    fun mapFrom(list: List<RecommendationTipViewModel>): List<RecommendationTipEntity> =
        list.map{ mapFrom(it) }
}