package com.smedialink.recommendation_list.di.module

import android.content.Context
import android.content.SharedPreferences
import com.smedialink.recommendation_list.data.local.source.sign.ISignLocalSource
import com.smedialink.recommendation_list.data.local.source.sign.SignLocalSource
import com.smedialink.recommendation_list.data.network.source.sign.ISignNetSource
import com.smedialink.recommendation_list.data.network.source.sign.SignNetSource
import com.smedialink.recommendation_list.data.repisitory.SignRepository
import com.smedialink.recommendation_list.di.scope.PerApplication
import com.smedialink.recommendation_list.domain.repository.ISignRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class SignModule {

    @PerApplication
    @Binds
    abstract fun bindSignNetworkSource(source: SignNetSource): ISignNetSource

    @PerApplication
    @Binds
    abstract fun bindSignLocalSource(source: SignLocalSource): ISignLocalSource

    @PerApplication
    @Binds
    abstract fun bindSignRepository(source: SignRepository): ISignRepository

    @Module
    companion object {

        const val PREF_NAME = "AppPrefName"

        @PerApplication
        @JvmStatic
        @Provides
        fun provideSharedPreferences(context: Context): SharedPreferences =
            context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }
}