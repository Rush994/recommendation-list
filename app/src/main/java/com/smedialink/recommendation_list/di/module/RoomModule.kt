package com.smedialink.recommendation_list.di.module

import android.content.Context
import androidx.room.Room
import com.smedialink.recommendation_list.data.db.AppDatabase
import com.smedialink.recommendation_list.data.db.dao.*
import com.smedialink.recommendation_list.di.scope.PerApplication
import dagger.Module
import dagger.Provides


@Module
class RoomModule {

    private lateinit var appDatabase: AppDatabase

    @PerApplication
    @Provides
    fun providesRoomDatabase(mContext: Context): AppDatabase {
        appDatabase = Room.databaseBuilder(mContext, AppDatabase::class.java, "RecommendationsDB").build()
        return appDatabase
    }

    @PerApplication
    @Provides
    fun providesRecommendationListDao(appDatabase: AppDatabase): RecommendationListDao {
        return appDatabase.getRecommendationListDao()
    }

    @PerApplication
    @Provides
    fun providesRecommendationTipsDao(appDatabase: AppDatabase): RecommendationTipsDao {
        return appDatabase.getRecommendationTipsDao()
    }

    @PerApplication
    @Provides
    fun providesRecommendationTagsDao(appDatabase: AppDatabase): RecommendationTagsDao {
        return appDatabase.getRecommendationTagsDao()
    }

    @PerApplication
    @Provides
    fun providesRecommendationTagJoinsDao(appDatabase: AppDatabase): RecommendationTagJoinsDao {
        return appDatabase.getRecommendationTagJoinsDao()
    }

    @PerApplication
    @Provides
    fun providesRecommendationSingleWithTipsDao(appDatabase: AppDatabase): RecommendationSingleWithTipsDao {
        return appDatabase.getRecommendationSingleWithTipsDao()
    }

    @PerApplication
    @Provides
    fun providesUserProfileDao(appDatabase: AppDatabase): UserProfileDao {
        return appDatabase.getUserProfileDao()
    }
}