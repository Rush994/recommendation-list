package com.smedialink.recommendation_list.di

import android.app.Application
import android.content.Context
import com.smedialink.recommendation_list.App
import com.smedialink.recommendation_list.di.module.*
import com.smedialink.recommendation_list.di.scope.PerApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@PerApplication
@Component(modules = arrayOf(
    AppModule::class,
    ActivityBuildersModule::class,
    ViewModelFactoryModule::class,
    NetworkModule::class,
    RoomModule::class,
    SignModule::class,
    ProfileModule::class,
    RecommendationsModule::class
    //MainModule::class
    ))
interface AppComponent: AndroidInjector<App> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(mContext: Context): Builder
        @BindsInstance
        fun networkModule(netModule: NetworkModule): Builder

        fun build(): AppComponent
    }
}