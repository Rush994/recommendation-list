package com.smedialink.recommendation_list.di.module

import com.smedialink.recommendation_list.data.db.source.profile.IProfileDBSource
import com.smedialink.recommendation_list.data.db.source.profile.ProfileDBSource
import com.smedialink.recommendation_list.data.network.source.profile.IProfileNetSource
import com.smedialink.recommendation_list.data.network.source.profile.ProfileNetSource
import com.smedialink.recommendation_list.data.repisitory.ProfileRepository
import com.smedialink.recommendation_list.di.scope.PerApplication
import com.smedialink.recommendation_list.domain.repository.IProfileRepository
import dagger.Binds
import dagger.Module

@Module
abstract class ProfileModule {
    @PerApplication
    @Binds
    abstract fun bindProfileNetSource(source: ProfileNetSource): IProfileNetSource

    @PerApplication
    @Binds
    abstract fun bindProfileDBSource(source: ProfileDBSource): IProfileDBSource

    @PerApplication
    @Binds
    abstract fun bindProfileRepository(source: ProfileRepository): IProfileRepository
}