package com.smedialink.recommendation_list.di.module

import com.smedialink.recommendation_list.data.db.source.recommendations.IRecommendationsDBSource
import com.smedialink.recommendation_list.data.db.source.recommendations.RecommendationsDBSource
import com.smedialink.recommendation_list.data.network.source.recommendations.IRecommendationsNetSource
import com.smedialink.recommendation_list.data.network.source.recommendations.RecommendationsNetSource
import com.smedialink.recommendation_list.data.repisitory.RecommendationsRepository
import com.smedialink.recommendation_list.di.scope.PerApplication
import com.smedialink.recommendation_list.domain.repository.IRecommendationsRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RecommendationsModule {

    @PerApplication
    @Binds
    abstract fun bindRecommendationsSource(source: RecommendationsNetSource): IRecommendationsNetSource

    @PerApplication
    @Binds
    abstract fun bindRecommendationsDBSource(source: RecommendationsDBSource): IRecommendationsDBSource

    @PerApplication
    @Binds
    abstract fun bindRecommendationsRepository(source: RecommendationsRepository): IRecommendationsRepository

}