package com.smedialink.recommendation_list.di.module

import com.smedialink.recommendation_list.BuildConfig
import com.smedialink.recommendation_list.data.network.api.Api
import com.smedialink.recommendation_list.data.network.api.Api.Companion.BASE_URL
import com.smedialink.recommendation_list.data.network.interceptor.AccessTokenInterceptor
import com.smedialink.recommendation_list.data.network.interceptor.RefreshTokenInterceptor
import com.smedialink.recommendation_list.di.scope.PerApplication
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule() {

    @PerApplication
    @Provides
    fun provideRetrofit(accessTokenInterceptor: AccessTokenInterceptor,
                        refreshTokenInterceptor: RefreshTokenInterceptor): Retrofit {

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE })
            .addInterceptor(accessTokenInterceptor).addInterceptor(refreshTokenInterceptor)

        return Retrofit.Builder()
            .client(httpClient.build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
    }

    @PerApplication
    @Provides
    fun provideApi(retrofit: Retrofit): Api =
        retrofit.create(Api::class.java)

}