package com.smedialink.recommendation_list.di.module

import com.smedialink.recommendation_list.di.scope.PerActivity
import com.smedialink.recommendation_list.presentation.feature.recommendationlist.RecommendationListActivity
import com.smedialink.recommendation_list.presentation.feature.recommendationlist.di.RecommendationListModule
import com.smedialink.recommendation_list.presentation.feature.recommendationsingle.RecommendationSingleActivity
import com.smedialink.recommendation_list.presentation.feature.recommendationsingle.di.RecommendationSingleModule
import com.smedialink.recommendation_list.presentation.feature.sign.SignActivity
import com.smedialink.recommendation_list.presentation.feature.sign.di.SignModule
import com.smedialink.recommendation_list.presentation.feature.signin.SignInActivity
import com.smedialink.recommendation_list.presentation.feature.signin.di.SignInModule
import com.smedialink.recommendation_list.presentation.feature.signup.SignUpActivity
import com.smedialink.recommendation_list.presentation.feature.signup.di.SignUpModule
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = arrayOf(AndroidInjectionModule::class))
abstract class ActivityBuildersModule {

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(SignModule::class))
    abstract fun contributeSignActivity(): SignActivity

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(SignInModule::class))
    abstract fun contributeSignInActivity(): SignInActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [(SignUpModule::class)])
    abstract fun contributeSignUpActivity(): SignUpActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [(RecommendationListModule::class)])
    abstract fun contributeRecommendationListActivity(): RecommendationListActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [(RecommendationSingleModule::class)])
    abstract fun contributeRecommendationSingleActivity(): RecommendationSingleActivity

}