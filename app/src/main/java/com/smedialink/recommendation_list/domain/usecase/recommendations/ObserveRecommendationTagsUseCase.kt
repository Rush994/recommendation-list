package com.smedialink.recommendation_list.domain.usecase.recommendations

import com.smedialink.recommendation_list.domain.repository.IRecommendationsRepository
import javax.inject.Inject

class ObserveRecommendationTagsUseCase @Inject constructor(private val recommendationsRepository: IRecommendationsRepository){

    operator fun invoke(recommendationId: Long) =
        recommendationsRepository.observeRecommendationTags(recommendationId)

}