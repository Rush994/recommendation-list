package com.smedialink.recommendation_list.domain.usecase.recommendations

import com.smedialink.recommendation_list.domain.entity.RecommendationTipEntity
import com.smedialink.recommendation_list.domain.repository.IRecommendationsRepository
import io.reactivex.rxjava3.core.Completable
import javax.inject.Inject

class AddRecommendationTipUseCase @Inject constructor(private val recommendationsRepository: IRecommendationsRepository) {
    operator fun invoke(recommendationId: Long, recommendationTipEntityList: List<RecommendationTipEntity>): Completable =
        recommendationsRepository.addRecommendationTipDB(recommendationId, recommendationTipEntityList)
}