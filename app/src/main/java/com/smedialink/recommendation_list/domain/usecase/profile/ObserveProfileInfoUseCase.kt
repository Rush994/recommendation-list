package com.smedialink.recommendation_list.domain.usecase.profile

import com.smedialink.recommendation_list.data.repisitory.ProfileRepository
import com.smedialink.recommendation_list.domain.entity.UserProfileEntity
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject


class ObserveProfileInfoUseCase @Inject constructor(private val profileRepository: ProfileRepository) {
    operator fun invoke(): Observable<UserProfileEntity> =
        profileRepository.observeProfileInfo()
}