package com.smedialink.recommendation_list.domain.entity

import com.google.gson.annotations.SerializedName
import com.smedialink.recommendation_list.data.network.response.RecommendationTagResp
import com.smedialink.recommendation_list.data.network.response.RecommendationTipResp

data class RecommendationSingleEntity(
    val id: Long = 0,
    val isDraft: Boolean = false,
    val photo: String? = null,
    val category: String = "",
    val header: String
)