package com.smedialink.recommendation_list.domain.entity

class RecommendationSingleWithTipsEntity(
    var single: RecommendationSingleEntity,
    var tips: List<RecommendationTipEntity>
)