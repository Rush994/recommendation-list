package com.smedialink.recommendation_list.domain.usecase.sign

import com.smedialink.recommendation_list.domain.repository.ISignRepository
import io.reactivex.rxjava3.core.Completable
import javax.inject.Inject

class SignInUseCase @Inject constructor(val signRepository: ISignRepository) {

    operator fun invoke(username: String, password: String): Completable =
        signRepository.signIn(username, password)

}