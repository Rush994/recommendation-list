package com.smedialink.recommendation_list.domain.common

abstract class Mapper <in E, T> {
    abstract fun mapFrom(item: E): T
}