package com.smedialink.recommendation_list.domain.usecase.sign

import com.smedialink.recommendation_list.domain.repository.IProfileRepository
import com.smedialink.recommendation_list.domain.repository.ISignRepository
import io.reactivex.rxjava3.core.Completable
import javax.inject.Inject

class SignUpUseCase @Inject constructor(private val signRepository: ISignRepository,
                                        private val profileRepository: IProfileRepository
) {

    operator fun invoke(firstName: String, lastName: String, userName: String, email: String, password: String, isActive: Boolean): Completable =
        signRepository.signUp(firstName, lastName, userName, email, password, isActive)
            .flatMapCompletable {
                profileRepository.updateProfileInfo(it)
            }
            .andThen(signRepository.signIn(userName,password))
}