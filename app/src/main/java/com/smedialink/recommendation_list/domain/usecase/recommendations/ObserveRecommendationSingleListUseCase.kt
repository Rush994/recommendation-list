package com.smedialink.recommendation_list.domain.usecase.recommendations

import com.smedialink.recommendation_list.domain.entity.RecommendationSingleEntity
import com.smedialink.recommendation_list.domain.repository.IRecommendationsRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class ObserveRecommendationSingleListUseCase @Inject constructor(private val recommendationsRepository: IRecommendationsRepository) {

    operator fun invoke(): Observable<List<RecommendationSingleEntity>> =
        recommendationsRepository.observeRecommendationSingleList()

}