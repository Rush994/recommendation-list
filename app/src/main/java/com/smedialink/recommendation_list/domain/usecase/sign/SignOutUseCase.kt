package com.smedialink.recommendation_list.domain.usecase.sign

import com.smedialink.recommendation_list.domain.repository.IProfileRepository
import com.smedialink.recommendation_list.domain.repository.IRecommendationsRepository
import com.smedialink.recommendation_list.domain.repository.ISignRepository
import io.reactivex.rxjava3.core.Completable
import javax.inject.Inject

class SignOutUseCase @Inject constructor(private val signRepository: ISignRepository,
                                         private val profileRepository: IProfileRepository,
                                         private val recommendationsRepository: IRecommendationsRepository
) {
    operator fun invoke(): Completable =
        signRepository.signOut()
            .andThen(profileRepository.clearProfileInfo())
            .andThen(recommendationsRepository.clearRecommendationList())
}