package com.smedialink.recommendation_list.domain.usecase.recommendations

import com.smedialink.recommendation_list.domain.entity.RecommendationSingleWithTipsEntity
import com.smedialink.recommendation_list.domain.repository.IRecommendationsRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class ObserveRecommendationSingleWithTipsUseCase @Inject constructor(private val recommendationsRepository: IRecommendationsRepository) {
    operator fun invoke(recommendationId: Long): Observable<RecommendationSingleWithTipsEntity> =
        recommendationsRepository.observeRecommendationSingleWithTips(recommendationId)
}