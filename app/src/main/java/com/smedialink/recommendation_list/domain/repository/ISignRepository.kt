package com.smedialink.recommendation_list.domain.repository

import com.smedialink.recommendation_list.domain.entity.UserProfileEntity
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface ISignRepository {

    fun signIn(userName: String,
               password: String): Completable

    fun signUp(firstName: String,
               lastName: String,
               userName: String,
               email: String,
               password: String,
               isActive: Boolean): Single<UserProfileEntity>

    fun signedInCheck(): Boolean

    fun signOut(): Completable
}