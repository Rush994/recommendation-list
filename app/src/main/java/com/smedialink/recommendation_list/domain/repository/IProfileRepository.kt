package com.smedialink.recommendation_list.domain.repository

import com.smedialink.recommendation_list.domain.entity.UserProfileEntity
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable

interface IProfileRepository {
    fun observeProfileInfo(): Observable<UserProfileEntity>

    fun updateProfileInfo(userProfileEntity: UserProfileEntity): Completable

    fun clearProfileInfo(): Completable
}