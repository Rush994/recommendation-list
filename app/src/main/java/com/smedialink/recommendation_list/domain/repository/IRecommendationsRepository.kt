package com.smedialink.recommendation_list.domain.repository

import com.smedialink.recommendation_list.domain.entity.*
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

interface IRecommendationsRepository {

    fun observeRecommendationSingleList(): Observable<List<RecommendationSingleEntity>>

    fun observeRecommendationSingleWithTips(recommendationId: Long): Observable<RecommendationSingleWithTipsEntity>

    fun observeRecommendationTags(recommendationId: Long): Observable<List<RecommendationTagEntity>>

    fun loadUserRecommendationList(userId: Long): Completable

    fun getRecommendationTip(listId: Int, tipId: Int): Single<RecommendationTipEntity>

    fun postUserRecommendationSingleNet(recommendationSingleEntity: RecommendationSingleEntity): Completable
    fun postUserRecommendationSingleDB(recommendationSingleEntity: RecommendationSingleEntity): Completable

    fun addRecommendationTipNet(recommendationId: Long, recommendationTipListEntity: List<RecommendationTipEntity>): Completable
    fun addRecommendationTipDB(recommendationId: Long, recommendationTipListEntity: List<RecommendationTipEntity>): Completable

    fun addRecommendationTagNet(recommendationId: Long, recommendTag: RecommendationTagEntity, recommendationTagEntityList: List<RecommendationTagEntity>): Completable
    fun addRecommendationTagDB(
        recommendationId: Long,
        recommendTag: RecommendationTagEntity,
        recommendationTagEntityList: List<RecommendationTagEntity>
    ): Completable

    fun deleteRecommendationSingleNet(recommendationId:Long): Completable
    fun deleteRecommendationSingleDB(recommendationId:Long): Completable

    fun clearRecommendationList(): Completable

}