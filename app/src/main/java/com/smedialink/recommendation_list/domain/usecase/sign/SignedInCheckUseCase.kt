package com.smedialink.recommendation_list.domain.usecase.sign

import com.smedialink.recommendation_list.domain.repository.ISignRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SignedInCheckUseCase @Inject constructor(val signRepository: ISignRepository){

    operator fun invoke(): Single<Boolean> =
        Single.fromCallable { signRepository.signedInCheck() }
}