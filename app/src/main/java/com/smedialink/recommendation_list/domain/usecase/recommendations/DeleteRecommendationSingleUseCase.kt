package com.smedialink.recommendation_list.domain.usecase.recommendations

import com.smedialink.recommendation_list.domain.repository.IRecommendationsRepository
import io.reactivex.rxjava3.core.Completable
import javax.inject.Inject

class DeleteRecommendationSingleUseCase @Inject constructor(private val recommendationsRepository: IRecommendationsRepository) {
    operator fun invoke(recommendationId: Long): Completable =
        recommendationsRepository.deleteRecommendationSingleDB(recommendationId)
}