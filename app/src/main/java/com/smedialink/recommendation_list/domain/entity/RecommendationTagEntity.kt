package com.smedialink.recommendation_list.domain.entity

data class RecommendationTagEntity(val name:String)