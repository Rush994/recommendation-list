package com.smedialink.recommendation_list.domain.usecase.recommendations

import com.smedialink.recommendation_list.domain.repository.IProfileRepository
import com.smedialink.recommendation_list.domain.repository.IRecommendationsRepository
import io.reactivex.rxjava3.core.Completable
import javax.inject.Inject

class LoadUserRecommendationListUseCase @Inject constructor(private val recommendationsRepository: IRecommendationsRepository,
                                                            private val profileRepository: IProfileRepository)  {
    operator fun invoke(): Completable =
        profileRepository.observeProfileInfo().flatMapCompletable {
            recommendationsRepository.loadUserRecommendationList(it.id)
        }
}