package com.smedialink.recommendation_list.domain.usecase.recommendations

import com.smedialink.recommendation_list.domain.entity.RecommendationSingleEntity
import com.smedialink.recommendation_list.domain.repository.IRecommendationsRepository
import io.reactivex.rxjava3.core.Completable
import javax.inject.Inject

class PostUserRecommendationSingleUseCase @Inject constructor(private val recommendationsRepository: IRecommendationsRepository) {
    operator fun invoke(item: RecommendationSingleEntity): Completable =
        recommendationsRepository.postUserRecommendationSingleDB(item)

}