package com.smedialink.recommendation_list.domain.entity

data class RecommendationTipEntity(
    val id: Long = 0,
    val text: String,
    val photo: String? = null
)