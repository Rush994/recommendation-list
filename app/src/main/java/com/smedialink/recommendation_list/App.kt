package com.smedialink.recommendation_list

import com.smedialink.recommendation_list.di.AppComponent
import com.smedialink.recommendation_list.di.DaggerAppComponent
import com.smedialink.recommendation_list.di.module.NetworkModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.DaggerApplication
import javax.inject.Inject

class App: DaggerApplication(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        component = DaggerAppComponent
            .builder()
            .context(this)
            .networkModule(NetworkModule())
            .build()
        return component
    }

    override fun androidInjector(): AndroidInjector<Any>  =
        androidInjector

    companion object{
        lateinit var component: AppComponent
    }
}