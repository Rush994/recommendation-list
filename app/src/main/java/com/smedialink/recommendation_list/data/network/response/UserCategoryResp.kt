package com.smedialink.recommendation_list.data.network.response

data class UserCategoryResp(val categoryTitle: String)