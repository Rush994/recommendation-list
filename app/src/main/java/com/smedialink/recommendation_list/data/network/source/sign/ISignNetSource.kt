package com.smedialink.recommendation_list.data.network.source.sign

import com.smedialink.recommendation_list.data.network.response.UserLoginResp
import com.smedialink.recommendation_list.data.network.response.UserProfileResp
import io.reactivex.rxjava3.core.Single

interface ISignNetSource {
    fun signIn(userName: String, password:String): Single<UserLoginResp>

    fun signUp(firstName: String,
               lastName: String,
               userName: String,
               email: String,
               password: String,
               isActive: Boolean): Single<UserProfileResp>
}