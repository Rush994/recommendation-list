package com.smedialink.recommendation_list.data.network.api.response.mapper

import com.smedialink.recommendation_list.data.db.model.RecommendationSingleWithTipsTagsModel
import com.smedialink.recommendation_list.data.db.model.mapper.RecommendationSingleRespModelMapper
import com.smedialink.recommendation_list.data.network.api.response.RecommendationListResp
import javax.inject.Inject

class RecommendationListRespModelMapper @Inject constructor(
    private val recommendationSingleRespModelMapper: RecommendationSingleRespModelMapper
) {
    fun mapFrom(item: RecommendationListResp): List<RecommendationSingleWithTipsTagsModel> =
        recommendationSingleRespModelMapper.mapFrom(item.recommendsList)
}