package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.db.model.RecommendationSingleModel
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationSingleEntity
import javax.inject.Inject

class RecommendationSingleModelEntityMapper @Inject constructor(): Mapper<RecommendationSingleModel, RecommendationSingleEntity>(){

    override fun mapFrom(item: RecommendationSingleModel): RecommendationSingleEntity =
        RecommendationSingleEntity(
            id = item.id,
            isDraft = item.isDraft,
            photo = item.photo,
            category = item.category,
            header = item.header)

    fun mapFrom(list: List<RecommendationSingleModel>): List<RecommendationSingleEntity> =
        list.map { mapFrom(it) }

}