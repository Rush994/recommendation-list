package com.smedialink.recommendation_list.data.db.model

import androidx.room.Embedded
import androidx.room.Relation

class RecommendationSingleWithTipsModel
{
    @Embedded
    lateinit var single: RecommendationSingleModel

    @Relation(parentColumn = "Id", entityColumn = "RecommendationId")
    var tips: List<RecommendationTipModel> = emptyList()
}