package com.smedialink.recommendation_list.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.smedialink.recommendation_list.data.db.dao.RecommendationListDao
import com.smedialink.recommendation_list.data.db.dao.RecommendationSingleWithTipsDao
import com.smedialink.recommendation_list.data.db.dao.RecommendationTagJoinsDao
import com.smedialink.recommendation_list.data.db.dao.RecommendationTagsDao
import com.smedialink.recommendation_list.data.db.dao.RecommendationTipsDao
import com.smedialink.recommendation_list.data.db.dao.UserProfileDao
import com.smedialink.recommendation_list.data.db.model.RecommendationSingleModel
import com.smedialink.recommendation_list.data.db.model.RecommendationTagJoinModel
import com.smedialink.recommendation_list.data.db.model.RecommendationTagModel
import com.smedialink.recommendation_list.data.db.model.RecommendationTipModel
import com.smedialink.recommendation_list.data.db.model.UserProfileModel

@Database(entities = arrayOf(
    UserProfileModel::class,
    RecommendationTipModel::class,
    RecommendationTagModel::class,
    RecommendationSingleModel::class,
    RecommendationTagJoinModel::class
),
    version = 1,
    exportSchema = false)
abstract class AppDatabase: RoomDatabase(){

    companion object{
        const val USER_TABLE = "ProfileInfo"
        const val RECOMMENDATION_SINGLE_TABLE = "RecommendationSingleInfo"
        const val RECOMMENDATION_TIPS_TABLE = "RecommendationTipInfo"
        const val RECOMMENDATION_TAGS_TABLE = "RecommendationTagInfo"
        const val RECOMMENDATION_JOIN_TABLE = "RecommendationTagJoinInfo"
    }

    abstract fun getUserProfileDao(): UserProfileDao

    abstract fun getRecommendationListDao(): RecommendationListDao

    abstract fun getRecommendationTipsDao(): RecommendationTipsDao

    abstract fun getRecommendationTagsDao(): RecommendationTagsDao

    abstract fun getRecommendationTagJoinsDao(): RecommendationTagJoinsDao

    abstract fun getRecommendationSingleWithTipsDao(): RecommendationSingleWithTipsDao

}