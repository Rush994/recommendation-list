package com.smedialink.recommendation_list.data.network.request.mapper

import com.smedialink.recommendation_list.data.network.request.RecommendationTagReq
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationTagEntity
import javax.inject.Inject

class RecommendationTagEntityReqMapper @Inject constructor(): Mapper<RecommendationTagEntity, RecommendationTagReq>() {
    override fun mapFrom(item: RecommendationTagEntity): RecommendationTagReq =
        RecommendationTagReq(item.name)

    fun mapFrom(list: List<RecommendationTagEntity>): List<RecommendationTagReq> =
        list.map { mapFrom(it) }
}