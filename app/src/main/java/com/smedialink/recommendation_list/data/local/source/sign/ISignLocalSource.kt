package com.smedialink.recommendation_list.data.local.source.sign

interface ISignLocalSource {

    fun updateAccessToken(token: String)
    fun updateRefreshToken(token: String)

    fun getAccessToken(): String?
    fun getRefreshToken(): String?

    fun clearToken(token: String)
    fun clearTokens()
}