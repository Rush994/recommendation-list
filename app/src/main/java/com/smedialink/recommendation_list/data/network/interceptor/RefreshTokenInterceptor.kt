package com.smedialink.recommendation_list.data.network.interceptor

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.smedialink.recommendation_list.data.local.source.sign.ISignLocalSource
import com.smedialink.recommendation_list.data.network.api.Api
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import javax.inject.Inject


class RefreshTokenInterceptor @Inject constructor(
    val signLocalSource: ISignLocalSource
) : Interceptor {

    private val composite = CompositeDisposable()

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        if (response.code != 401) {
            return response
        }

        response.close()
        val newTokenResponse = getNewAccessToken(chain)
        if (newTokenResponse.code == 401) {
            Completable.complete()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ }, {
                    Log.d("Refresh error", it.message!!)
                }).also { composite.add(it) }
            return response
        }
        val token = parseAccessToken(newTokenResponse)
        signLocalSource.updateAccessToken(token)
        return retryLastRequest(chain)
    }

    @Throws(IOException::class)
    private fun retryLastRequest(chain: Interceptor.Chain): Response {
        var request = chain.request()

        val access = signLocalSource.getAccessToken()
        access?.let {
            request = request.newBuilder()
                .addHeader(
                    "Authorization",
                    "jwt $it"
                )//$it")
                .build()
        }
        return chain.proceed(request)
    }

    @Throws(IOException::class)
    private fun getNewAccessToken(chain: Interceptor.Chain): Response {
        val requestString = "{\"refresh_token\":\"${signLocalSource.getRefreshToken()}\"}"

        val body = requestString.toRequestBody(requestString.toMediaTypeOrNull())
        val request = chain.request()
            .newBuilder()
            .url("${Api.BASE_URL}${Api.REFRESH_PART}")
            .post(body)
            .removeHeader("Authorization")
            .build()
        return chain.proceed(request)
    }

    @Throws(IOException::class)
    private fun parseAccessToken(refreshResponse: Response): String {
        return refreshResponse.body?.let {
            return Gson().fromJson(it.string(), JsonObject::class.java)
                .get("access")
                .asString
        } ?: ""
    }

}