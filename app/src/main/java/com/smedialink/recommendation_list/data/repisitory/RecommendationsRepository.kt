package com.smedialink.recommendation_list.data.repisitory

import com.smedialink.recommendation_list.data.db.model.mapper.RecommendationSingleEntityModelMapper
import com.smedialink.recommendation_list.data.db.model.mapper.RecommendationSingleModelEntityMapper
import com.smedialink.recommendation_list.data.db.model.mapper.RecommendationSingleRespModelMapper
import com.smedialink.recommendation_list.data.db.model.mapper.RecommendationSingleWithTipsModelEntityMapper
import com.smedialink.recommendation_list.data.db.model.mapper.RecommendationTagEntityModelMapper
import com.smedialink.recommendation_list.data.db.model.mapper.RecommendationTagEntityRespMapper
import com.smedialink.recommendation_list.data.db.model.mapper.RecommendationTagModelEntityMapper
import com.smedialink.recommendation_list.data.db.model.mapper.RecommendationTipRespModelMapper
import com.smedialink.recommendation_list.data.db.source.recommendations.IRecommendationsDBSource
import com.smedialink.recommendation_list.data.network.api.response.mapper.RecommendationListRespModelMapper
import com.smedialink.recommendation_list.data.network.request.mapper.RecommendationSingleEntityReqMapper
import com.smedialink.recommendation_list.data.network.request.mapper.RecommendationTagEntityReqMapper
import com.smedialink.recommendation_list.data.network.request.mapper.RecommendationTipEntityReqMapper
import com.smedialink.recommendation_list.data.network.response.mapper.RecommendationTipRespEntityMapper
import com.smedialink.recommendation_list.data.network.source.recommendations.IRecommendationsNetSource
import com.smedialink.recommendation_list.domain.entity.RecommendationSingleEntity
import com.smedialink.recommendation_list.domain.entity.RecommendationSingleWithTipsEntity
import com.smedialink.recommendation_list.domain.entity.RecommendationTagEntity
import com.smedialink.recommendation_list.domain.entity.RecommendationTipEntity
import com.smedialink.recommendation_list.domain.repository.IRecommendationsRepository
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class RecommendationsRepository @Inject constructor(
    private val recommendationsNetSource: IRecommendationsNetSource,
    private val recommendationsDBSource: IRecommendationsDBSource,
    private val recommendationTagEntityRespMapper: RecommendationTagEntityRespMapper,
    private val recommendationsTipRespModelMapper: RecommendationTipRespModelMapper,
    private val recommendationTipRespEntityMapper: RecommendationTipRespEntityMapper,
    private val recommendationSingleModelEntityMapper: RecommendationSingleModelEntityMapper,
    private val recommendationSingleEntityModelMapper: RecommendationSingleEntityModelMapper,
    private val recommendationListRespModelMapper: RecommendationListRespModelMapper,
    private val recommendationSingleRespModelMapper: RecommendationSingleRespModelMapper,
    private val recommendationSingleWithTipsModelEntityMapper: RecommendationSingleWithTipsModelEntityMapper,
    private val recommendationTagEntityVMMapper: RecommendationTagModelEntityMapper,
    private val recommendationTipEntityReqMapper: RecommendationTipEntityReqMapper,
    private val recommendationTagEntityReqMapper: RecommendationTagEntityReqMapper,
    private val recommendationTagEntityModelMapper: RecommendationTagEntityModelMapper,
    private val recommendationSingleEntityReqMapper: RecommendationSingleEntityReqMapper
) : IRecommendationsRepository {

    override fun observeRecommendationSingleList(): Observable<List<RecommendationSingleEntity>> =
        recommendationsDBSource.observeRecommendationSingle()
            .map {
                recommendationSingleModelEntityMapper.mapFrom(it)
            }

    override fun observeRecommendationSingleWithTips(recommendationId: Long): Observable<RecommendationSingleWithTipsEntity> =
        recommendationsDBSource.observeRecommendationSingleWithTips(recommendationId)
            .map {
                recommendationSingleWithTipsModelEntityMapper.mapFrom(it)
            }

    override fun observeRecommendationTags(recommendationId: Long): Observable<List<RecommendationTagEntity>> =
        recommendationsDBSource.observeRecommendationTags(recommendationId)
            .map {
                recommendationTagEntityVMMapper.mapFrom(it)
            }

    override fun loadUserRecommendationList(userId: Long): Completable =
        recommendationsNetSource.getUserRecommendationList(userId)
            .map {
                recommendationListRespModelMapper.mapFrom(it)
            }
            .flatMapCompletable { resp ->
                recommendationsDBSource.insertRecommendationList(resp)
            }

    override fun getRecommendationTip(listId: Int, tipId: Int): Single<RecommendationTipEntity> =
        recommendationsNetSource.getRecommendationTip(listId, tipId)
            .map {
                recommendationTipRespEntityMapper.mapFrom(it)
            }


    override fun postUserRecommendationSingleNet(recommendationSingleEntity: RecommendationSingleEntity): Completable =
        recommendationsNetSource.postUserRecommendationSingle(
            recommendationSingleEntityReqMapper.mapFrom(
                recommendationSingleEntity
            )
        )
            .map {
                recommendationSingleRespModelMapper.mapFrom(it)
            }
            .flatMapCompletable { resp ->
                recommendationsDBSource.insertRecommendationSingle(resp.single)
            }

    override fun postUserRecommendationSingleDB(recommendationSingleEntity: RecommendationSingleEntity): Completable =
        recommendationsDBSource.insertRecommendationSingle(
            recommendationSingleEntityModelMapper.mapFrom(recommendationSingleEntity))

    override fun addRecommendationTipNet(
        recommendationId: Long,
        recommendationTipListEntity: List<RecommendationTipEntity>): Completable =
        recommendationsNetSource.addRecommendationTip(
            recommendationId,
            recommendationTipEntityReqMapper.mapFrom(recommendationTipListEntity)
        )
            .map { recommendationSingleRespModelMapper.mapFrom(it) }
            .flatMapCompletable { resp ->
                recommendationsDBSource.addRecommendationTips(resp.tips)
            }
    override fun addRecommendationTipDB(
        recommendationId: Long,
        recommendationTipListEntity: List<RecommendationTipEntity>): Completable =
        recommendationsDBSource.addRecommendationTips(recommendationsTipRespModelMapper.mapFrom(
            recommendationTagEntityRespMapper.mapFrom(recommendationTipListEntity), recommendationId))

    override fun addRecommendationTagNet(
        recommendationId: Long,
        recommendTag: RecommendationTagEntity,
        recommendationTagEntityList: List<RecommendationTagEntity>
    ): Completable =
        recommendationsNetSource.addRecommendationTag(
            recommendationId,
            recommendationTagEntityReqMapper.mapFrom(recommendationTagEntityList)
        )
            .map { recommendationSingleRespModelMapper.mapFrom(it) }
            .flatMapCompletable { resp ->
                recommendationsDBSource.addRecommendationTag(
                    resp.single.id,
                    recommendationTagEntityModelMapper.mapFrom(recommendTag)
                )
            }
    override fun addRecommendationTagDB(
        recommendationId: Long,
        recommendTag: RecommendationTagEntity,
        recommendationTagEntityList: List<RecommendationTagEntity>
    ): Completable =
        recommendationsDBSource.addRecommendationTag(
            recommendationId,
            recommendationTagEntityModelMapper.mapFrom(recommendTag)
        )

    override fun deleteRecommendationSingleNet(recommendationId: Long): Completable =
        recommendationsNetSource.deleteRecommendationSingle(recommendationId)
            .andThen(recommendationsDBSource.deleteRecommendationSingle(recommendationId))

    override fun deleteRecommendationSingleDB(recommendationId: Long): Completable =
        recommendationsDBSource.deleteRecommendationSingle(recommendationId)

    override fun clearRecommendationList(): Completable =
        recommendationsDBSource.clearRecommendationList()
}