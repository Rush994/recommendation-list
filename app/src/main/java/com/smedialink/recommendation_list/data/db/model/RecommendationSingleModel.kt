package com.smedialink.recommendation_list.data.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.smedialink.recommendation_list.data.db.AppDatabase

@Entity(tableName = AppDatabase.RECOMMENDATION_SINGLE_TABLE)
data class RecommendationSingleModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Id")
    val id: Long,
    @ColumnInfo(name = "IsDraft")
    val isDraft: Boolean,
    @ColumnInfo(name = "Photo")
    val photo: String?,
    @ColumnInfo(name = "Category")
    val category: String,
    @ColumnInfo(name = "Header")
    val header: String

)