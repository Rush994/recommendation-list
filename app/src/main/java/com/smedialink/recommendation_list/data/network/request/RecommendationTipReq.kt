package com.smedialink.recommendation_list.data.network.request

import com.google.gson.annotations.SerializedName

data class RecommendationTipReq(
    @SerializedName("text")
    val text: String,
    @SerializedName("photo")
    val photo: String? = null
    )