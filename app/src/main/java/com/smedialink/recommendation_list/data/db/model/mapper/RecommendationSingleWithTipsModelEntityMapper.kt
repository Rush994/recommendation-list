package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.db.model.RecommendationSingleWithTipsModel
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationSingleWithTipsEntity
import javax.inject.Inject

class RecommendationSingleWithTipsModelEntityMapper @Inject constructor(
    private val recommendationSingleModelEntityMapper: RecommendationSingleModelEntityMapper,
    private val recommendationTipModelEntityMapper: RecommendationTipModelEntityMapper
) :
    Mapper<RecommendationSingleWithTipsModel, RecommendationSingleWithTipsEntity>() {
    override fun mapFrom(item: RecommendationSingleWithTipsModel): RecommendationSingleWithTipsEntity =
        RecommendationSingleWithTipsEntity(
            recommendationSingleModelEntityMapper.mapFrom(item.single),
            recommendationTipModelEntityMapper.mapFrom(item.tips))


    fun mapFrom(list: List<RecommendationSingleWithTipsModel>): List<RecommendationSingleWithTipsEntity> =
        list.map { mapFrom(it) }

}