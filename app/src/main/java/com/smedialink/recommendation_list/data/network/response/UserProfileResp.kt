package com.smedialink.recommendation_list.data.network.response

import com.google.gson.annotations.SerializedName

data class UserProfileResp(
    @SerializedName("id")
    val id: Long,
    @SerializedName("email")
    val email: String,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("username")
    val userName: String,
    @SerializedName("is_active")
    val isActive: Boolean,
    @SerializedName("avatar")
    val avatar: String?)
