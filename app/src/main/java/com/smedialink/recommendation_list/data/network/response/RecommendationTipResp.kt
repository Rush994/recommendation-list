package com.smedialink.recommendation_list.data.network.response

import com.google.gson.annotations.SerializedName


data class RecommendationTipResp(
    @SerializedName("id")
    val id: Long,

    @SerializedName("text")
    val text: String,

    @SerializedName("photo")
    val photo: String?)