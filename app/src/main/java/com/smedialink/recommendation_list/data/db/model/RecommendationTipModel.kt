package com.smedialink.recommendation_list.data.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.smedialink.recommendation_list.data.db.AppDatabase

@Entity(tableName = AppDatabase.RECOMMENDATION_TIPS_TABLE,
    foreignKeys = [
        ForeignKey(
            entity = RecommendationSingleModel::class,
            parentColumns = ["Id"],
            childColumns = ["RecommendationId"],
            onDelete = ForeignKey.CASCADE)])
data class RecommendationTipModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Id")
    var id: Long,
    @ColumnInfo(name = "Text")
    val text: String,
    @ColumnInfo(name = "Photo")
    val photo: String?,

    @ColumnInfo(name = "RecommendationId")
    val recommendationId: Long = -1
)