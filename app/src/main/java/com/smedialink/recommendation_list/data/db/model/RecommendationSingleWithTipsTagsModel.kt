package com.smedialink.recommendation_list.data.db.model

class RecommendationSingleWithTipsTagsModel(
    val single: RecommendationSingleModel,
    val tips: List<RecommendationTipModel>,
    val tags: List<RecommendationTagModel>
)