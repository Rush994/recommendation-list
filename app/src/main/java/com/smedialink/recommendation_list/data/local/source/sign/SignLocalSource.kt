package com.smedialink.recommendation_list.data.local.source.sign

import android.content.SharedPreferences
import javax.inject.Inject

class SignLocalSource @Inject constructor(private val prefs: SharedPreferences): ISignLocalSource {

    companion object{
        const val ACCESS_TOKEN = "AccessToken"
        const val REFRESH_TOKEN = "RefreshToken"
    }

    override fun updateAccessToken(token: String) {
        prefs.edit().putString(ACCESS_TOKEN, token).apply()
    }

    override fun updateRefreshToken(token: String) {
        prefs.edit().putString(REFRESH_TOKEN, token).apply()
    }

    override fun getAccessToken(): String? =
        //prefs.getString(ACCESS_TOKEN, null)
        "111111111111111"

    override fun getRefreshToken(): String? =
        prefs.getString(REFRESH_TOKEN, null)

    override fun clearToken(token: String) {
        prefs.edit().remove(token).apply()
    }

    override fun clearTokens() {
        prefs.edit().remove(ACCESS_TOKEN).apply()
        prefs.edit().remove(REFRESH_TOKEN).apply()
    }
}