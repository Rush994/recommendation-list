package com.smedialink.recommendation_list.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.smedialink.recommendation_list.data.db.AppDatabase
import com.smedialink.recommendation_list.data.db.model.RecommendationTagJoinModel
import com.smedialink.recommendation_list.data.db.model.RecommendationTagModel
import io.reactivex.rxjava3.core.Observable

@Dao
abstract class RecommendationTagJoinsDao: BaseDao<RecommendationTagJoinModel>(AppDatabase.RECOMMENDATION_TIPS_TABLE) {

    @Query("""SELECT * FROM RecommendationTagInfo INNER JOIN RecommendationTagJoinInfo ON
                    RecommendationTagInfo.Id=RecommendationTagJoinInfo.recommendTagId WHERE
                    RecommendationTagJoinInfo.recommendationId=:recommendationId""")
    abstract fun observeUserCars(recommendationId: Long): Observable<List<RecommendationTagModel>>


}