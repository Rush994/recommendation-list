package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.db.model.UserProfileModel
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.UserProfileEntity
import javax.inject.Inject

class UserProfileEntityModelMapper @Inject constructor(): Mapper<UserProfileEntity, UserProfileModel>() {
    override fun mapFrom(item: UserProfileEntity): UserProfileModel =
        UserProfileModel(
            id = item.id,
            email = item.email,
            firstName = item.firstName,
            lastName = item.lastName,
            userName = item.userName,
            isActive = item.isActive,
            avatar = item.avatar)
}