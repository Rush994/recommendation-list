package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.db.model.RecommendationTagModel
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationTagEntity
import javax.inject.Inject

class RecommendationTagEntityModelMapper @Inject constructor(): Mapper<RecommendationTagEntity, RecommendationTagModel>() {
    override fun mapFrom(item: RecommendationTagEntity): RecommendationTagModel =
        RecommendationTagModel(name = item.name)
}