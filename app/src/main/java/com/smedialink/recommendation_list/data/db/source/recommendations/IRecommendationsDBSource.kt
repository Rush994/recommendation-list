package com.smedialink.recommendation_list.data.db.source.recommendations

import com.smedialink.recommendation_list.data.db.model.RecommendationSingleModel
import com.smedialink.recommendation_list.data.db.model.RecommendationSingleWithTipsModel
import com.smedialink.recommendation_list.data.db.model.RecommendationSingleWithTipsTagsModel
import com.smedialink.recommendation_list.data.db.model.RecommendationTagModel
import com.smedialink.recommendation_list.data.db.model.RecommendationTipModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable

interface IRecommendationsDBSource {

    fun insertRecommendationList(recommendationList: List<RecommendationSingleWithTipsTagsModel>): Completable

    fun insertRecommendationSingle(recommendationSingleModel: RecommendationSingleModel): Completable

    fun observeRecommendationSingle(): Observable<List<RecommendationSingleModel>>

    fun observeRecommendationSingleWithTips(recommendationId: Long): Observable<RecommendationSingleWithTipsModel>

    fun observeRecommendationTags(recommendationId: Long): Observable<List<RecommendationTagModel>>

    fun addRecommendationTips(recommendationTipModelList: List<RecommendationTipModel>): Completable

    fun addRecommendationTag(recommendationId: Long, recommendTag: RecommendationTagModel): Completable

    fun deleteRecommendationSingle(recommendationId:Long): Completable

    fun clearRecommendationList(): Completable
}