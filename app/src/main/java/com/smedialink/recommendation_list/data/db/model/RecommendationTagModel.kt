package com.smedialink.recommendation_list.data.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.smedialink.recommendation_list.data.db.AppDatabase

@Entity(tableName = AppDatabase.RECOMMENDATION_TAGS_TABLE)
data class RecommendationTagModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Id")
    val id: Long = 0,
    @ColumnInfo(name = "Name")
    val name: String
)