package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.db.model.RecommendationTagModel
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationTagEntity
import javax.inject.Inject

class RecommendationTagModelEntityMapper @Inject constructor(): Mapper<RecommendationTagModel, RecommendationTagEntity>() {
    override fun mapFrom(item: RecommendationTagModel): RecommendationTagEntity =
        RecommendationTagEntity(item.name)

    fun mapFrom(list: List<RecommendationTagModel>): List<RecommendationTagEntity> =
        list.map { mapFrom(it) }
}