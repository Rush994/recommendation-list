package com.smedialink.recommendation_list.data.db.source.sign

import com.smedialink.recommendation_list.data.db.dao.UserProfileDao
import com.smedialink.recommendation_list.data.db.model.UserProfileModel
import io.reactivex.rxjava3.core.Completable
import javax.inject.Inject

class SignDBSource @Inject constructor(private val userProfileDao: UserProfileDao): ISignDBSource {
    override fun insertUser(userProfileModel: UserProfileModel): Completable =
        Completable.fromAction {
            userProfileDao.insertOrReplace(userProfileModel)
        }
}