package com.smedialink.recommendation_list.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.smedialink.recommendation_list.data.db.AppDatabase
import com.smedialink.recommendation_list.data.db.model.UserProfileModel
import io.reactivex.rxjava3.core.Observable

@Dao
abstract class UserProfileDao: BaseDao<UserProfileModel>(AppDatabase.USER_TABLE) {

    @Query("SELECT * FROM ProfileInfo LIMIT 1")
    abstract fun observeProfileInfo(): Observable<UserProfileModel>

    @Query("SELECT * FROM ProfileInfo LIMIT 1")
    abstract fun getProfileInfo(): UserProfileModel
}