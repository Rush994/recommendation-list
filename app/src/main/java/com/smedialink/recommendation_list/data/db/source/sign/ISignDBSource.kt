package com.smedialink.recommendation_list.data.db.source.sign

import com.smedialink.recommendation_list.data.db.model.UserProfileModel
import io.reactivex.rxjava3.core.Completable

interface ISignDBSource {
    fun insertUser(userProfileModel: UserProfileModel): Completable
}