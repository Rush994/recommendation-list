package com.smedialink.recommendation_list.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.smedialink.recommendation_list.data.db.AppDatabase
import com.smedialink.recommendation_list.data.db.model.RecommendationSingleModel
import io.reactivex.rxjava3.core.Observable

@Dao
abstract class RecommendationListDao: BaseDao<RecommendationSingleModel>(AppDatabase.RECOMMENDATION_SINGLE_TABLE) {
    @Query("SELECT * FROM RecommendationSingleInfo")
    abstract fun observeRecommendations(): Observable<List<RecommendationSingleModel>>

    @Query("DELETE FROM RecommendationSingleInfo WHERE Id=:recommendationId")
    abstract fun deleteById(recommendationId: Long)
}