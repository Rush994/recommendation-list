package com.smedialink.recommendation_list.data.db.dao

import androidx.room.Dao
import com.smedialink.recommendation_list.data.db.AppDatabase
import com.smedialink.recommendation_list.data.db.model.RecommendationTagModel

@Dao
abstract class RecommendationTagsDao: BaseDao<RecommendationTagModel>(AppDatabase.RECOMMENDATION_TAGS_TABLE) {

}