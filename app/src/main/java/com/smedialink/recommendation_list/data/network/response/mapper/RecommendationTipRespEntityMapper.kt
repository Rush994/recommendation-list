package com.smedialink.recommendation_list.data.network.response.mapper

import com.smedialink.recommendation_list.data.network.response.RecommendationTipResp
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationTipEntity
import javax.inject.Inject

class RecommendationTipRespEntityMapper @Inject constructor() :
    Mapper<RecommendationTipResp, RecommendationTipEntity>() {
    override fun mapFrom(item: RecommendationTipResp): RecommendationTipEntity =
        RecommendationTipEntity(
            id = item.id,
            text = item.text,
            photo = item.photo
        )

    fun mapFrom(list: List<RecommendationTipResp>): List<RecommendationTipEntity> =
        list.map { mapFrom(it) }
}