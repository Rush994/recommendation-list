package com.smedialink.recommendation_list.data.network.response.mapper

import com.smedialink.recommendation_list.data.network.response.RecommendationTagResp
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationTagEntity
import javax.inject.Inject

class RecommendationTagRespEntityMapper @Inject constructor() :
    Mapper<RecommendationTagResp, RecommendationTagEntity>() {
    override fun mapFrom(item: RecommendationTagResp): RecommendationTagEntity =
        RecommendationTagEntity(item.name)

    fun mapFrom(list: List<RecommendationTagResp>): List<RecommendationTagEntity> =
        list.map { mapFrom(it) }
}