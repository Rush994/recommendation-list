package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.network.response.RecommendationTipResp
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationTipEntity
import javax.inject.Inject

class RecommendationTagEntityRespMapper @Inject constructor(): Mapper<RecommendationTipEntity, RecommendationTipResp>() {
    override fun mapFrom(item: RecommendationTipEntity): RecommendationTipResp =
        RecommendationTipResp(
            id = item.id,
            text = item.text,
            photo = null)

    fun mapFrom(list: List<RecommendationTipEntity>): List<RecommendationTipResp> =
        list.map { mapFrom(it) }
}