package com.smedialink.recommendation_list.data.network.response

import com.google.gson.annotations.SerializedName

data class UserLoginResp(
    @SerializedName("access")
    val accessToken: String,

    @SerializedName("refresh")
    val refreshToken: String
)