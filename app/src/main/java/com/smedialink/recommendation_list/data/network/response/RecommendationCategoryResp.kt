package com.smedialink.recommendation_list.data.network.response

data class RecommendationCategoryResp(
    val categoryTitle: String)