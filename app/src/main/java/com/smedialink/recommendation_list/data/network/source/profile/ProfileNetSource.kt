package com.smedialink.recommendation_list.data.network.source.profile

import com.smedialink.recommendation_list.data.network.api.Api
import com.smedialink.recommendation_list.data.network.response.UserProfileResp
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class ProfileNetSource @Inject constructor(private val api: Api): IProfileNetSource {
    override fun getCurrentUser(): Single<UserProfileResp> =
        api.getCurrentUser()

}