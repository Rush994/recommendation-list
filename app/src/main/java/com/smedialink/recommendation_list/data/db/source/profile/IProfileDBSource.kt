package com.smedialink.recommendation_list.data.db.source.profile

import com.smedialink.recommendation_list.data.db.model.UserProfileModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable

interface IProfileDBSource {

    fun getProfileInfo(): UserProfileModel

    fun observeProfileInfo(): Observable<UserProfileModel>

    fun updateProfileInfo(userProfileModel: UserProfileModel): Completable

    fun clearProfileInfo(): Completable
}