package com.smedialink.recommendation_list.data.network.request.mapper

import com.smedialink.recommendation_list.data.network.request.RecommendationTipReq
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationTipEntity
import javax.inject.Inject

class RecommendationTipEntityReqMapper @Inject constructor(
) : Mapper<RecommendationTipEntity, RecommendationTipReq>() {
    override fun mapFrom(item: RecommendationTipEntity): RecommendationTipReq =
        RecommendationTipReq(item.text)

    fun mapFrom(list: List<RecommendationTipEntity>): List<RecommendationTipReq> =
        list.map { mapFrom(it) }
}