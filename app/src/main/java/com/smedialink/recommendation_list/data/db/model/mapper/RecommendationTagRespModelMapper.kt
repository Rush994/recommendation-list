package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.db.model.RecommendationTagModel
import com.smedialink.recommendation_list.data.network.response.RecommendationTagResp
import javax.inject.Inject

class RecommendationTagRespModelMapper @Inject constructor(){
    fun mapFrom(item: RecommendationTagResp): RecommendationTagModel =
        RecommendationTagModel(name = item.name)

    fun mapFrom(list: List<RecommendationTagResp>): List<RecommendationTagModel> =
        list.map { mapFrom(it) }
}