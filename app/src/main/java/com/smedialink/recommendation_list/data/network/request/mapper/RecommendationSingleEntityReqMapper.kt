package com.smedialink.recommendation_list.data.network.request.mapper

import com.smedialink.recommendation_list.data.network.request.RecommendationSingleReq
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationSingleEntity
import javax.inject.Inject

class RecommendationSingleEntityReqMapper @Inject constructor(): Mapper<RecommendationSingleEntity, RecommendationSingleReq>(){
    override fun mapFrom(item: RecommendationSingleEntity): RecommendationSingleReq =
        RecommendationSingleReq(header = item.header)

}