package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.db.model.RecommendationTipModel
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationTipEntity
import javax.inject.Inject

class RecommendationTipModelEntityMapper @Inject constructor() : Mapper<RecommendationTipModel, RecommendationTipEntity>() {
    override fun mapFrom(item: RecommendationTipModel): RecommendationTipEntity =
        RecommendationTipEntity(
            id = item.id,
            text = item.text,
            photo = item.photo)
    fun mapFrom(list: List<RecommendationTipModel>): List<RecommendationTipEntity> =
        list.map { mapFrom(it) }

}