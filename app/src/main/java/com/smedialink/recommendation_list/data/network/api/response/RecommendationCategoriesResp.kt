package com.smedialink.recommendation_list.data.network.api.response

import com.smedialink.recommendation_list.data.network.response.RecommendationCategoryResp

data class RecommendationCategoriesResp(val categories: List<RecommendationCategoryResp>)