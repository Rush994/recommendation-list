package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.db.model.RecommendationSingleModel
import com.smedialink.recommendation_list.data.db.model.RecommendationSingleWithTipsTagsModel
import com.smedialink.recommendation_list.data.network.response.RecommendationSingleResp
import javax.inject.Inject

class RecommendationSingleRespModelMapper @Inject constructor(
    private val recommendationsTipRespModelMapper: RecommendationTipRespModelMapper,
    private val recommendationsTagRespModelMapper: RecommendationTagRespModelMapper
) {

    fun mapFrom(item: RecommendationSingleResp): RecommendationSingleWithTipsTagsModel =
        RecommendationSingleWithTipsTagsModel(
            single =
            RecommendationSingleModel(
                id = item.id,
                isDraft = item.isDraft,
                photo = item.photo,
                category = item.category,
                header = item.header
            ),
            tips = recommendationsTipRespModelMapper.mapFrom(item.tips, item.id),
            tags = recommendationsTagRespModelMapper.mapFrom(item.tags)
        )

    fun mapFrom(list: List<RecommendationSingleResp>): List<RecommendationSingleWithTipsTagsModel> =
        list.map { mapFrom(it) }
}