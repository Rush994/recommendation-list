package com.smedialink.recommendation_list.data.network.source.recommendations

import com.smedialink.recommendation_list.data.network.api.response.RecommendationCategoriesResp
import com.smedialink.recommendation_list.data.network.api.response.RecommendationListResp
import com.smedialink.recommendation_list.data.network.request.RecommendationSingleReq
import com.smedialink.recommendation_list.data.network.request.RecommendationTagReq
import com.smedialink.recommendation_list.data.network.request.RecommendationTipReq
import com.smedialink.recommendation_list.data.network.response.RecommendationSingleResp
import com.smedialink.recommendation_list.data.network.response.RecommendationTipResp
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface IRecommendationsNetSource {

    fun getRecommendationTip(listId: Int, tipId: Int): Single<RecommendationTipResp>

    fun postUserRecommendationSingle(recommendationSingleReq: RecommendationSingleReq): Single<RecommendationSingleResp>

    fun getRecommendationCategories(): Single<List<RecommendationCategoriesResp>>

    fun addRecommendationTip(recommendationId: Long, recommendationTipListReq: List<RecommendationTipReq>): Single<RecommendationSingleResp>

    fun addRecommendationTag(recommendationId: Long, recommendationTagListReq: List<RecommendationTagReq>): Single<RecommendationSingleResp>

    fun getUserRecommendationList(userId:Long): Single<RecommendationListResp>

    fun deleteRecommendationSingle(recommendationId:Long): Completable

}