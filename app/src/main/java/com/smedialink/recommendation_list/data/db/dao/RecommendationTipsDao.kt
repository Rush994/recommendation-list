package com.smedialink.recommendation_list.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.smedialink.recommendation_list.data.db.AppDatabase
import com.smedialink.recommendation_list.data.db.model.RecommendationTipModel
import io.reactivex.rxjava3.core.Observable

@Dao
abstract class RecommendationTipsDao: BaseDao<RecommendationTipModel>(AppDatabase.RECOMMENDATION_TIPS_TABLE) {
    @Query("SELECT * FROM RecommendationTipInfo WHERE RecommendationId = :recommendId")
    abstract fun observeTips(recommendId: Long): Observable<RecommendationTipModel>
}