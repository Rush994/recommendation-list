package com.smedialink.recommendation_list.data.repisitory

import com.smedialink.recommendation_list.data.local.source.sign.SignLocalSource
import com.smedialink.recommendation_list.data.network.response.mapper.UserProfileRespEntityMapper
import com.smedialink.recommendation_list.data.network.source.sign.SignNetSource
import com.smedialink.recommendation_list.domain.entity.UserProfileEntity
import com.smedialink.recommendation_list.domain.repository.ISignRepository
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Completable.fromAction
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SignRepository @Inject constructor(private val signNetSource: SignNetSource,
                                         private val signLocSource: SignLocalSource,
                                         val userProfileRespEntityMapper: UserProfileRespEntityMapper
) :
    ISignRepository {

    override fun signIn(userName: String, password: String): Completable =
        signNetSource.signIn(userName, password)
            .flatMapCompletable { resp ->
                fromAction {
                    signLocSource.updateAccessToken(resp.accessToken)
                    signLocSource.updateRefreshToken(resp.refreshToken)
                }
            }

    override fun signUp(firstName: String, lastName: String, userName: String, email: String, password: String, isActive: Boolean): Single<UserProfileEntity> =
        signNetSource.signUp(firstName, lastName, userName, email, password, isActive)
            .map { userProfileRespEntityMapper.mapFrom(it) }


    override fun signedInCheck(): Boolean =
        signLocSource.getAccessToken() != null

    override fun signOut(): Completable =
        fromAction {
            signLocSource.clearTokens()
        }
}