package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.db.model.RecommendationTipModel
import com.smedialink.recommendation_list.data.network.response.RecommendationTipResp
import javax.inject.Inject

class RecommendationTipRespModelMapper @Inject constructor(){
    fun mapFrom(item: RecommendationTipResp, id: Long): RecommendationTipModel =
        RecommendationTipModel(
            id = item.id,
            text = item.text,
            photo = item.photo,
            recommendationId = id)

    fun mapFrom(list: List<RecommendationTipResp>, id: Long): List<RecommendationTipModel> =
        list.map { mapFrom(it, id) }
}