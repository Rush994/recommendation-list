package com.smedialink.recommendation_list.data.network.request

import com.google.gson.annotations.SerializedName

data class RecommendationSingleReq(
    @SerializedName("recommendations")
    val recommendations: List<RecommendationTipReq> = listOf(RecommendationTipReq("default tip")),

    @SerializedName("is_draft")
    val isDraft: Boolean = false,

    @SerializedName("photo")
    val photo: String? = null,

    @SerializedName("category")
    val category: String = "free theme",

    @SerializedName("header")
    val header: String)