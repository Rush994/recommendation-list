package com.smedialink.recommendation_list.data.db.source.recommendations

import com.smedialink.recommendation_list.data.db.dao.RecommendationListDao
import com.smedialink.recommendation_list.data.db.dao.RecommendationSingleWithTipsDao
import com.smedialink.recommendation_list.data.db.dao.RecommendationTagJoinsDao
import com.smedialink.recommendation_list.data.db.dao.RecommendationTagsDao
import com.smedialink.recommendation_list.data.db.dao.RecommendationTipsDao
import com.smedialink.recommendation_list.data.db.model.RecommendationSingleModel
import com.smedialink.recommendation_list.data.db.model.RecommendationSingleWithTipsModel
import com.smedialink.recommendation_list.data.db.model.RecommendationSingleWithTipsTagsModel
import com.smedialink.recommendation_list.data.db.model.RecommendationTagJoinModel
import com.smedialink.recommendation_list.data.db.model.RecommendationTagModel
import com.smedialink.recommendation_list.data.db.model.RecommendationTipModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Completable.fromAction
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class RecommendationsDBSource @Inject constructor(
    private val recommendationListDao: RecommendationListDao,
    private val recommendationTipsDao: RecommendationTipsDao,
    private val recommendationTagsDao: RecommendationTagsDao,
    private val recommendationSingleWithTipsDao: RecommendationSingleWithTipsDao,
    private val recommendationTagJoinsDao: RecommendationTagJoinsDao
) : IRecommendationsDBSource {

    override fun observeRecommendationTags(recommendationId: Long): Observable<List<RecommendationTagModel>> =
        recommendationTagJoinsDao.observeUserCars(recommendationId)

    override fun insertRecommendationList(recommendationList: List<RecommendationSingleWithTipsTagsModel>): Completable =
        fromAction {
            val singleList: MutableList<RecommendationSingleModel> = mutableListOf()
            val tipList: MutableList<RecommendationTipModel> = mutableListOf()
            val tagList: MutableList<RecommendationTagModel> = mutableListOf()
            var recTagJoinList: MutableList<RecommendationTagJoinModel> = mutableListOf()

            recommendationList.forEach {
                singleList.add(it.single)
                tipList.addAll(it.tips)
                tagList.addAll(it.tags)

                for (item in it.tags)
                    recTagJoinList.add(RecommendationTagJoinModel(it.single.id, 0))
            }

            recommendationListDao.insertOrReplace(singleList)
            recommendationTipsDao.insertOrReplace(tipList)
            val ids = recommendationTagsDao.insertOrReplace(tagList)

            val map =
                recTagJoinList.zip(ids).toMap()
            for (item in map)
                item.key.recommendTagId = item.value

            recTagJoinList = map.keys.toMutableList()
            recommendationTagJoinsDao.insertOrReplace(recTagJoinList)
        }

    override fun insertRecommendationSingle(recommendationSingleModel: RecommendationSingleModel): Completable =
    fromAction{
        val id = recommendationListDao.insertOrReplace(recommendationSingleModel)
        var i = 1
    }
//        fromAction {
//            recommendationListDao.insertOrReplace(recommendationSingle.single)
//            recommendationTipsDao.insertOrReplace(recommendationSingle.tips)
//        }

    override fun observeRecommendationSingle(): Observable<List<RecommendationSingleModel>> =
        recommendationListDao.observeRecommendations()

    override fun observeRecommendationSingleWithTips(recommendationId: Long): Observable<RecommendationSingleWithTipsModel> =
        recommendationSingleWithTipsDao.observeRecommendationsWithTips(recommendationId)

    override fun addRecommendationTips(recommendationTipModelList: List<RecommendationTipModel>): Completable =
        fromAction {
            recommendationTipsDao.insertOrReplace(recommendationTipModelList)
        }

    override fun addRecommendationTag(recommendationId: Long, recommendTag: RecommendationTagModel): Completable =
        fromAction {
            val id = recommendationTagsDao.insertOrReplace(recommendTag)
            val recTagJoin = RecommendationTagJoinModel(recommendationId, id)
            recommendationTagJoinsDao.insertOrReplace(recTagJoin)
        }

    override fun deleteRecommendationSingle(recommendationId:Long): Completable =
        fromAction {
            recommendationListDao.deleteById(recommendationId)
        }

    override fun clearRecommendationList(): Completable =
        fromAction{
            recommendationListDao.deleteAll()
        }
}