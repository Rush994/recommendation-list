package com.smedialink.recommendation_list.data.network.source.sign

import com.smedialink.recommendation_list.data.network.api.Api
import com.smedialink.recommendation_list.data.network.response.UserLoginResp
import com.smedialink.recommendation_list.data.network.response.UserProfileResp
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SignNetSource @Inject constructor(private val api: Api): ISignNetSource {

    override fun signIn(userName: String, password: String): Single<UserLoginResp> =
        api.signIn(userName, password)

    override fun signUp(firstName: String, lastName: String,
                        userName: String, email: String, password: String, isActive: Boolean): Single<UserProfileResp> =
        api.signUp(firstName, lastName, userName, email, password, isActive)
}