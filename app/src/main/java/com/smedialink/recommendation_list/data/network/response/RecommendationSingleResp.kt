package com.smedialink.recommendation_list.data.network.response

import com.google.gson.annotations.SerializedName

data class RecommendationSingleResp(
    @SerializedName("id")
    val id: Long,

    @SerializedName("recommendations")
    val tips: List<RecommendationTipResp>,

    @SerializedName("tags")
    val tags: List<RecommendationTagResp>,

    @SerializedName("user")
    val user: UserProfileResp,

    @SerializedName("is_draft")
    val isDraft: Boolean,

    @SerializedName("photo")
    val photo: String?,

    @SerializedName("category")
    val category: String,

    @SerializedName("header")
    val header: String
)