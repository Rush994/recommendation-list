package com.smedialink.recommendation_list.data.network.response.mapper

import com.smedialink.recommendation_list.data.network.response.RecommendationSingleResp
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationSingleEntity
import javax.inject.Inject

class RecommendationsRespEntityMapper @Inject constructor(
) : Mapper<RecommendationSingleResp, RecommendationSingleEntity>() {
    override fun mapFrom(item: RecommendationSingleResp): RecommendationSingleEntity =
        RecommendationSingleEntity(
            id = item.id,
            isDraft = item.isDraft,
            photo = item.photo,
            category = item.category,
            header = item.header
        )

}