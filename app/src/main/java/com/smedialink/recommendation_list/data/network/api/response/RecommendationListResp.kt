package com.smedialink.recommendation_list.data.network.api.response

import com.google.gson.annotations.SerializedName
import com.smedialink.recommendation_list.data.network.response.RecommendationSingleResp

data class RecommendationListResp(
    @SerializedName("count")
    val count: Int,
    @SerializedName("results")
    val recommendsList: List<RecommendationSingleResp>)