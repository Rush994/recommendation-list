package com.smedialink.recommendation_list.data.network.response

import com.google.gson.annotations.SerializedName

data class RecommendationTagResp(
    @SerializedName("name")
    val name:String)