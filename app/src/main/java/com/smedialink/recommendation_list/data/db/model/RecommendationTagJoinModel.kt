package com.smedialink.recommendation_list.data.db.model

import androidx.room.Entity
import androidx.room.ForeignKey
import com.smedialink.recommendation_list.data.db.AppDatabase

@Entity(
    tableName = AppDatabase.RECOMMENDATION_JOIN_TABLE,
    primaryKeys = (arrayOf("recommendationId", "recommendTagId")),
    foreignKeys = [
        ForeignKey(
            entity = RecommendationSingleModel::class,
            parentColumns = arrayOf("Id"),
            childColumns = arrayOf("recommendationId"),
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = RecommendationTagModel::class,
            parentColumns = arrayOf("Id"),
            childColumns = arrayOf("recommendTagId"),
            onDelete = ForeignKey.CASCADE
        )])
class RecommendationTagJoinModel(
    var recommendationId: Long,
    var recommendTagId: Long
)