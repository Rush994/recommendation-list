package com.smedialink.recommendation_list.data.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.smedialink.recommendation_list.data.db.AppDatabase

@Entity(tableName = AppDatabase.USER_TABLE)
data class UserProfileModel(
    @PrimaryKey
    val id: Long,
    @ColumnInfo(name = "Email")
    val email: String,
    @ColumnInfo(name = "FirstName")
    val firstName: String,
    @ColumnInfo(name = "LastName")
    val lastName: String,
    @ColumnInfo(name = "Username")
    val userName: String,
    @ColumnInfo(name = "IsActive")
    val isActive: Boolean,
    @ColumnInfo(name = "Avatar")
    val avatar: String?
)