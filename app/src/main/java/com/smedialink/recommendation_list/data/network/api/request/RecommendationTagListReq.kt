package com.smedialink.recommendation_list.data.network.api.request

import com.google.gson.annotations.SerializedName
import com.smedialink.recommendation_list.data.network.request.RecommendationTagReq

class RecommendationTagListReq(
    @SerializedName("tags")
    val tags: List<RecommendationTagReq>
)