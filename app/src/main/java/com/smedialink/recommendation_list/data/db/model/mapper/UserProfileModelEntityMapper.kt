package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.db.model.UserProfileModel
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.UserProfileEntity
import javax.inject.Inject

class UserProfileModelEntityMapper @Inject constructor(): Mapper<UserProfileModel, UserProfileEntity>(){
    override fun mapFrom(item: UserProfileModel): UserProfileEntity =
        UserProfileEntity(
            id = item.id,
            email = item.email,
            firstName = item.firstName,
            lastName = item.lastName,
            userName = item.userName,
            isActive = item.isActive,
            avatar = item.avatar)

}