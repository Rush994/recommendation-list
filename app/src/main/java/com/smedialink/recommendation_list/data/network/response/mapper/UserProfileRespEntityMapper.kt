package com.smedialink.recommendation_list.data.network.response.mapper

import com.smedialink.recommendation_list.data.network.response.UserProfileResp
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.UserProfileEntity
import javax.inject.Inject

class UserProfileRespEntityMapper @Inject constructor() :
    Mapper<UserProfileResp, UserProfileEntity>() {
    override fun mapFrom(item: UserProfileResp): UserProfileEntity =
        UserProfileEntity(id = item.id, email = item.email, firstName = item.firstName,
            lastName = item.lastName, userName = item.userName, isActive = item.isActive, avatar = item.avatar)
}