package com.smedialink.recommendation_list.data.network.interceptor

import com.smedialink.recommendation_list.data.local.source.sign.ISignLocalSource
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AccessTokenInterceptor @Inject constructor(private val signLocalSource: ISignLocalSource):Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder()
            .build()

        val access = signLocalSource.getAccessToken()
        access?.let {
            request = request.newBuilder()
            .addHeader("Authorization", "jwt $it")
                .build()
        }

        return chain.proceed(request)
    }
}