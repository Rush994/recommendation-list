package com.smedialink.recommendation_list.data.network.response

import com.google.gson.annotations.SerializedName

data class UserRefreshResp(
    @SerializedName("access")
    val accessToken: String)