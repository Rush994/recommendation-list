package com.smedialink.recommendation_list.data.network.request

import com.google.gson.annotations.SerializedName

data class RecommendationTagReq(
    @SerializedName("name")
    val name: String)