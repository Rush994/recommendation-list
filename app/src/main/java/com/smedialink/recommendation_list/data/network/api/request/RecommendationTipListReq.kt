package com.smedialink.recommendation_list.data.network.api.request

import com.google.gson.annotations.SerializedName
import com.smedialink.recommendation_list.data.network.request.RecommendationTipReq

data class RecommendationTipListReq(
    @SerializedName("recommendations")
    val tips: List<RecommendationTipReq>
)