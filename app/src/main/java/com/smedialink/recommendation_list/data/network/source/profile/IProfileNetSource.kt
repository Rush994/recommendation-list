package com.smedialink.recommendation_list.data.network.source.profile

import com.smedialink.recommendation_list.data.network.response.UserProfileResp
import io.reactivex.rxjava3.core.Single

interface IProfileNetSource {
    fun getCurrentUser(): Single<UserProfileResp>
}