package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.db.model.RecommendationSingleModel
import com.smedialink.recommendation_list.domain.common.Mapper
import com.smedialink.recommendation_list.domain.entity.RecommendationSingleEntity
import javax.inject.Inject

class RecommendationSingleEntityModelMapper @Inject constructor(): Mapper<RecommendationSingleEntity, RecommendationSingleModel>() {
    override fun mapFrom(item: RecommendationSingleEntity): RecommendationSingleModel =
        RecommendationSingleModel(
            id = item.id,
            header = item.header,
            isDraft = false,
            photo = null,
            category = "default")
}