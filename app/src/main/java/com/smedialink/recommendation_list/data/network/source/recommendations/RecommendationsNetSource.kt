package com.smedialink.recommendation_list.data.network.source.recommendations

import com.smedialink.recommendation_list.data.network.api.Api
import com.smedialink.recommendation_list.data.network.api.request.RecommendationTagListReq
import com.smedialink.recommendation_list.data.network.api.request.RecommendationTipListReq
import com.smedialink.recommendation_list.data.network.api.response.RecommendationCategoriesResp
import com.smedialink.recommendation_list.data.network.api.response.RecommendationListResp
import com.smedialink.recommendation_list.data.network.request.RecommendationSingleReq
import com.smedialink.recommendation_list.data.network.request.RecommendationTagReq
import com.smedialink.recommendation_list.data.network.request.RecommendationTipReq
import com.smedialink.recommendation_list.data.network.response.RecommendationSingleResp
import com.smedialink.recommendation_list.data.network.response.RecommendationTipResp
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class RecommendationsNetSource @Inject constructor(private val api: Api) : IRecommendationsNetSource {
    override fun getUserRecommendationList(userId: Long): Single<RecommendationListResp> =
        api.getUserRecommendationList(userId)

    override fun getRecommendationTip(listId: Int, tipId: Int): Single<RecommendationTipResp> =
        api.getRecommendationTip(listId, tipId)

    override fun postUserRecommendationSingle(recommendationSingleReq: RecommendationSingleReq): Single<RecommendationSingleResp> =
        api.postRecommendationSingle(recommendationSingleReq)

    override fun getRecommendationCategories(): Single<List<RecommendationCategoriesResp>> =
        api.getRecommendationCategories()

    override fun addRecommendationTip(recommendationId: Long, recommendationTipListReq: List<RecommendationTipReq>): Single<RecommendationSingleResp> =
        api.addRecommendationTip(recommendationId, RecommendationTipListReq(recommendationTipListReq))

    override fun addRecommendationTag(recommendationId: Long, recommendationTagListReq: List<RecommendationTagReq>): Single<RecommendationSingleResp> =
        api.updateRecommendationTagList(recommendationId, RecommendationTagListReq(recommendationTagListReq))

    override fun deleteRecommendationSingle(recommendationId:Long): Completable =
        api.deleteRecommendation(recommendationId)

}