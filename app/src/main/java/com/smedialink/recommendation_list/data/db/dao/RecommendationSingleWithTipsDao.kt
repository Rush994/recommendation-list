package com.smedialink.recommendation_list.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.smedialink.recommendation_list.data.db.model.RecommendationSingleWithTipsModel
import io.reactivex.rxjava3.core.Observable

@Dao
abstract class RecommendationSingleWithTipsDao {
    @Query("SELECT * from RecommendationSingleInfo WHERE Id = :recommendationId")
    abstract fun observeRecommendationsWithTips(recommendationId: Long): Observable<RecommendationSingleWithTipsModel>
}