package com.smedialink.recommendation_list.data.repisitory

import com.smedialink.recommendation_list.data.db.model.UserProfileModel
import com.smedialink.recommendation_list.data.db.model.mapper.UserProfileEntityModelMapper
import com.smedialink.recommendation_list.data.db.model.mapper.UserProfileModelEntityMapper
import com.smedialink.recommendation_list.data.db.model.mapper.UserProfileRespModelMapper
import com.smedialink.recommendation_list.data.db.source.profile.IProfileDBSource
import com.smedialink.recommendation_list.data.network.source.profile.IProfileNetSource
import com.smedialink.recommendation_list.domain.entity.UserProfileEntity
import com.smedialink.recommendation_list.domain.repository.IProfileRepository
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class ProfileRepository @Inject constructor(
    private val profileDBSource: IProfileDBSource,
    private val profileNetSource: IProfileNetSource,
    private val userProfileModelEntityMapper: UserProfileModelEntityMapper,
    private val userProfileEntityModelMapper: UserProfileEntityModelMapper,
    private val userProfileRespModelMapper: UserProfileRespModelMapper
) : IProfileRepository {

    override fun observeProfileInfo(): Observable<UserProfileEntity> =
        Observable.create{ emitter -> emitter.onNext(UserProfileEntity(id = 1, email = "example@mail.ru",
            firstName = "firstName", lastName = "lastName", userName = "fav_user",
            isActive = true, avatar = null)) }
//        Observable.merge(profileDBSource.observeProfileInfo(), fetchUserFromNetwork())
//            .debounce(100, TimeUnit.MILLISECONDS)
//            .map {
//                userProfileModelEntityMapper.mapFrom(it)
//            }

    override fun updateProfileInfo(userProfileEntity: UserProfileEntity): Completable =
        profileDBSource.updateProfileInfo(userProfileEntityModelMapper.mapFrom(userProfileEntity))

    private fun fetchUserFromNetwork(): Observable<UserProfileModel> =
        profileNetSource.getCurrentUser()
            .map {
                userProfileRespModelMapper.mapFrom(it)
            }
            .flatMap { profile ->
                profileDBSource.updateProfileInfo(profile)
                    .toSingleDefault(profile)
            }
            .onErrorResumeNext {
                val cachedUser = profileDBSource.getProfileInfo()
                if (cachedUser != null) {
                    Single.just(cachedUser)
                } else {
                    Single.error(it)
                }
            }
            .toObservable()

    override fun clearProfileInfo(): Completable =
        profileDBSource.clearProfileInfo()
}