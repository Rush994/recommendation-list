package com.smedialink.recommendation_list.data.db.source.profile

import com.smedialink.recommendation_list.data.db.dao.UserProfileDao
import com.smedialink.recommendation_list.data.db.model.UserProfileModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Completable.fromAction
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class ProfileDBSource @Inject constructor(private val userProfileDao: UserProfileDao) :
    IProfileDBSource {
    override fun clearProfileInfo(): Completable =
        fromAction{
            userProfileDao.deleteAll()
        }

        override fun getProfileInfo(): UserProfileModel =
            userProfileDao.getProfileInfo()

        override fun updateProfileInfo(userProfileModel: UserProfileModel): Completable =
            fromAction {
                userProfileDao.insertOrReplace(userProfileModel)
            }

        override fun observeProfileInfo(): Observable<UserProfileModel> =
            userProfileDao.observeProfileInfo()
    }