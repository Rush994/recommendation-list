package com.smedialink.recommendation_list.data.network.api

import com.smedialink.recommendation_list.data.network.api.request.RecommendationTagListReq
import com.smedialink.recommendation_list.data.network.api.request.RecommendationTipListReq
import com.smedialink.recommendation_list.data.network.api.response.RecommendationCategoriesResp
import com.smedialink.recommendation_list.data.network.api.response.RecommendationListResp
import com.smedialink.recommendation_list.data.network.request.RecommendationSingleReq
import com.smedialink.recommendation_list.data.network.response.RecommendationSingleResp
import com.smedialink.recommendation_list.data.network.response.RecommendationTipResp
import com.smedialink.recommendation_list.data.network.response.UserLoginResp
import com.smedialink.recommendation_list.data.network.response.UserProfileResp
import com.smedialink.recommendation_list.data.network.response.UserRefreshResp
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {
    companion object{
        const val BASE_URL = "http://192.168.11.53:8000"
        const val REFRESH_PART = "/api/auth/refresh/"
    }

    @FormUrlEncoded
    @POST("/api/auth/login/")
    fun signIn(@Field("username") userName: String,
               @Field("password") password: String): Single<UserLoginResp>

    @FormUrlEncoded
    @POST("/api/auth/registration/")
    fun signUp(@Field("first_name")
               firstName: String,
               @Field("last_name")
               lastName: String,
               @Field("username")
               userName: String,
               @Field("email")
               email: String,
               @Field("password")
               password: String,
               @Field("is_active")
               isActive: Boolean): Single<UserProfileResp>

    @FormUrlEncoded
    @POST("/api/auth/logout/")
    fun logOut(@Field("id") id: Int): Completable

    @FormUrlEncoded
    @POST("/api/auth/refresh/")
    fun refresh(@Query("refresh_token") token: String): Single<UserRefreshResp>

    @GET("/api/users/me/")
    fun getCurrentUser(): Single<UserProfileResp>

    @GET("/recommendations/categories/")
    fun getRecommendationCategories(): Single<List<RecommendationCategoriesResp>>

    @GET("/api/users/")
    fun getUsers(): Single<List<UserProfileResp>>

    @GET("/api/recommendations/")
    fun getUserRecommendationList(@Query("user_id") id: Long): Single<RecommendationListResp>


    @FormUrlEncoded
    @GET("/api/recommendations/{id}/tips/{tip_id}/")
    fun getRecommendationTip(@Path("id") id: Int,
                             @Path("tip_id") tip_id: Int): Single<RecommendationTipResp>

    @PATCH("/api/recommendations/{id}/")
    fun addRecommendationTip(@Path("id") recommendationId: Long,
                             @Body recommendationTips: RecommendationTipListReq): Single<RecommendationSingleResp>

    @PATCH("/api/recommendations/{id}/")
    fun updateRecommendationTagList(@Path("id") recommendationId: Long,
                             @Body recommendationTips: RecommendationTagListReq
    ): Single<RecommendationSingleResp>

    @POST("/api/recommendations/")
    fun postRecommendationSingle(@Body recommendationSingleReq: RecommendationSingleReq): Single<RecommendationSingleResp>

    @DELETE("api/recommendations/{id}/")
    fun deleteRecommendation(@Path("id") recommendationId: Long): Completable
}