package com.smedialink.recommendation_list.data.db.model.mapper

import com.smedialink.recommendation_list.data.db.model.UserProfileModel
import com.smedialink.recommendation_list.data.network.response.UserProfileResp
import com.smedialink.recommendation_list.domain.common.Mapper
import javax.inject.Inject

class UserProfileRespModelMapper @Inject constructor() : Mapper<UserProfileResp, UserProfileModel>() {
    override fun mapFrom(item: UserProfileResp): UserProfileModel =
        UserProfileModel(
            id = item.id,
            email = item.email,
            firstName = item.firstName,
            lastName = item.lastName,
            userName = item.userName,
            isActive = item.isActive,
            avatar = item.avatar)
}